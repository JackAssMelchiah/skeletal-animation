/**********************************
* Game.hpp
* Main Game loop, which manages creation and destruction of scenes within game
* It is possible o change camera type to fps(used for capturing images within game) within coresponding scene
* for more info about class mehods, please check out .cpp file
*/

#ifndef  GAME_HPP
#define GAME_HPP

#include <string>

#include "Scene.hpp"
#include "Model.hpp"
#include "Player.hpp"
#include "SkeletalModel.hpp"


class BulletDebug;
class Game{

public:
	
	Game(SDL_Window* window, int screenWidth, int screenHeight, std::string resource_path);
	~Game();
	bool CreateGameScene();
	void SetToGame();
	void Run();
	void FreeScene(Scene* s);	

private:
	void MainLoop();
	void Refresh();
	void ClearScreen();
	void UpdateSceneFrameTime(Scene* s);
	void DestroyPlayer(Player *p);

	int screenWidth;
	int screenHeight;

	std::string resourcePath;
	Scene* gameScene;
	Scene* activeScene;

	SDL_Window * mainWindow;
	Player* mainPlayer;
};

#endif // ! GAME_HPP