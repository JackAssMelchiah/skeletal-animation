/**********************************
* Timer.hpp
* Measures time
**for more info about class mehods, please check out .cpp file
*/

#ifndef TIMER_HPP
#define TIMER_HPP

#include <SDL.h>

class Timer
{
public:
	Timer();
	void Start();
	void Pause();
	void Stop();
	void Unpause();
	Uint32 GetTicks();
	bool IsPaused();
	bool IsStarted();
	~Timer();

	/* data */
private:
	bool started;
	bool paused;
	Uint32 startTicks;
	Uint32 pausedTicks;
};
#endif

