/***************************
*Class Shader
*Complete abstraction of Shader, class loads and compiles shader
*When using this class, this shaderobject will always need to have atleast vertex and fragment shader
*Currently no support for compute and tesalation shaders 
* * for more info about class mehods, please check out .cpp file
*/

#ifndef GENERICSHADER_HPP
#define GENERICSHADER_HPP


#define NO_SDL_GLEXT
#define GLM_FORCE_RADIANS 
#include <GL/glew.h>
#include <SDL.h>
#include <string>
#include <iostream>

#include <stdlib.h>
#include <stdio.h>
#include <vector>


#define VERTEX_SHADER 0
#define GEOMETRY_SHADER 1
#define FRAGMENT_SHADER 2


using namespace std;

class Shader{
	
public:
	Shader();
	~Shader();
	bool LoadShaderProgram(string vertexShaderPath, string geometryShaderPath, string fragmentShaderPath);
	GLuint LoadShaders(string path, int type, bool *was_ok);
	void PrintfShaderLog(GLuint shader);
	void PrintProgramLog(GLuint program);
	GLuint GetProgram();

	/* data */
	GLuint programID;

private:
	bool ShaderFromFile(string shaderPath, string & src);
	bool loaded_shader;
};
#endif

