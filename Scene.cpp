#include "Scene.hpp"

/*Player must be the main player!!!, if player has no camera bound to it, then Scene will create one and bound it to it */
Scene::Scene(Player* p, SDL_Window* window) {

	AddCamera(p->GetBoundCamera());
	this->window = window;
	this->mainPlayer = p;
	this->activeCameraIndex = 0;
	this->globalLight = glm::vec3(0.f);
	this->globalLightPos = glm::vec3(0.f,50.f,0.f);
}


/*Renders all visible bounded object, returns true if ok, false when exit is inment*/
bool Scene::DrawScene() {

	//player input
	if (!this->mainPlayer->UpdateInputs())
		return false;
	this->mainPlayer->CalculateMotion();

	glm::mat4 vp;
	glm::mat4 wv;

	
	if (this->mainPlayer->GetMode() == Player::CamMode::FP){
		wv = glm::mat4(1.f);
		vp =glm::scale(
				glm::rotate(
		            glm::rotate(
		                glm::translate(
		                    this->sceneCameras[this->activeCameraIndex]->GetViewToProjection(),
		                    glm::vec3(0, 0, this->mainPlayer->getMove().z)
		                    ),
		                this->mainPlayer->getMove().y, glm::vec3(1, 0, 0)
		                ),
		            this->mainPlayer->getMove().x, glm::vec3(0, 1, 0)
		         ), this->mainPlayer->GetScaleFactor());
		}
		
	
	else {
		vp = this->sceneCameras[this->activeCameraIndex]->GetViewToProjection();
		wv = this->sceneCameras[this->activeCameraIndex]->GetWorldToViewMatrix();
	}

	std::map<int, Model*>::iterator m_it;
	for (m_it = this->sceneModels.begin(); m_it != this->sceneModels.end(); ++m_it){
		
		this->sceneModels[(m_it->first)]->UpdateMatrix();
		this->sceneModels[(m_it->first)]->Render(
		vp,
		wv,
		this->globalLight,
		this->globalLightPos,
		this->sceneCameras[this->activeCameraIndex]->GetCamPos()
		);	
	}
	return true;
}


void Scene::RemovePlayer(){

	this->mainPlayer = NULL;
}


void Scene::AddPlayer(Player* p){

	this->mainPlayer = p;
}


/*Adds model to scene, adds model and its instances that are CURENTLY in it to physics scene*/
int Scene::AddModel(Model* m) {

	this->sceneModels[m->GetId()] = m;
	//this->sceneModels.insert(std::pair<m->GetId(), *m>);
	return m->GetId();
}


/*Adds Camera to scene, returns an index to it*/
int Scene::AddCamera(Camera* c) {
	this->sceneCameras[c->GetId()] = c;
	//this->sceneCameras.insert(std::pair<c->GetId(), *c>);
	return c->GetId();
}


/*Removes model, so Scene cant use it, but  itstill remains in memory*/
bool Scene::RemoveModel(int index) {

	if (!(this->sceneModels.find(index) == this->sceneModels.end())) {
		this->sceneModels.erase(index);
		return true;
	} 
	return false;
}


/*Removes camera, so Scene cant use it, but it still remains in memory*/
bool Scene::RemoveCamera(int index) {

	if (!(this->sceneCameras.find(index) == this->sceneCameras.end())) {
		this->sceneCameras.erase(index);
		return true;
	} 
	return false;
}


/*Removes and destroys model in memory*/
bool Scene::DestroyModel(int index) {

	if (!(this->sceneModels.find(index) == this->sceneModels.end())) {
		delete this->sceneModels[index];
		this->sceneModels.erase(index);
		return true;
	} 
	return false;
}


/*Removes and destroys camera in memory*/
bool Scene::DestroyCamera(int index) {

	if (!(this->sceneCameras.find(index) == this->sceneCameras.end())) {
		delete this->sceneCameras[index];
		this->sceneCameras.erase(index);
		return true;
	} 
	return false;
}


/*Destructor will only remove models,... (wont destroy them)  */
Scene::~Scene() {

	std::map<int,Model*>::iterator m_it;
	std::map<int,Camera*>::iterator c_it;

	for(auto i: this->sceneModels){

		delete i.second;
	}
	this->sceneModels.clear();
}
