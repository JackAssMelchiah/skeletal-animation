#include "Model.hpp"



//////////DEBUG THIS

/**Method opens XML file, loads vertices, normals, uvs and indexes them **/
bool Model::OpenXMLModel(string filename, std::vector<glm::vec3>& vertOut, std::vector<glm::vec2>& uvOut, std::vector<glm::vec3>& normalOut) {

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<long> indices;

	glm::vec3 vertexVec;
	glm::vec3 normalVec;
	glm::vec2 uvVec;


	xmlDoc* document;

	xmlNode* vertex;
	xmlNode* faces;
	xmlNode* root_element;
	xmlNode* shaderGeometry;
	xmlNode* currentTag;
	xmlChar* xmlArray;


	int countOfModels = 0;
	float indice = 0.0f;


	document = xmlReadFile(filename.c_str(), "UTF-8", 0);//XML_PARSE_RECOVER
	if (document == NULL)
	{
		xmlCleanupParser();
		return false;
	}

	root_element = xmlDocGetRootElement(document);
	if (root_element == NULL)
	{
		std::cout << "bad xml!!" << std::endl;
		xmlFreeDoc(document);
		xmlCleanupParser();
		return false;
	}

	shaderGeometry = root_element->xmlChildrenNode;

	//get Count of geometry models, perhaps delete it?
	do
	{
		if (!xmlStrcmp(shaderGeometry->name, (const xmlChar*) "sharedgeometry"))
		{
			++countOfModels;
		}
		shaderGeometry = shaderGeometry->next;
	} while (shaderGeometry->next != NULL);

	//get the first one to be loaded

	vertex = root_element->children->next->children->next->children->next;
	currentTag = vertex;

	while (1)
	{
		//position in xml

		currentTag = vertex;
		currentTag = vertex->children->next;
		while (1)
		{ //new from here delete and replace with older if something is wrong
			if (!xmlStrcmp((const xmlChar *)"position", currentTag->name))
			{
				//vertices
				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"x");
				vertexVec.x = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"y");
				vertexVec.y = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"z");
				vertexVec.z = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				vertices.push_back(vertexVec);
			}
			else if (!xmlStrcmp((const xmlChar *)"normal", currentTag->name))
			{
				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"x");
				normalVec.x = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"y");
				normalVec.y = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"z");
				normalVec.z = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				normals.push_back(normalVec);
			}
			else if (!xmlStrcmp((const xmlChar *)"texcoord", currentTag->name))
			{
				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"u");
				uvVec.x = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"v");
				uvVec.y = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				uvs.push_back(uvVec);
			}

			if (currentTag->next->next == NULL)
				break;
			currentTag = currentTag->next->next;
		}

		if (vertex->next->next == NULL)
			break;
		vertex = vertex->next->next;
	}

	// faces
	faces = root_element->children->next->next->next->children->next->children->next->children->next;
	while (1)
	{
		xmlArray = xmlGetProp(faces, (const xmlChar *)"v1");
		indice = strtof((const char*)xmlArray, NULL);
		indices.push_back(indice);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(faces, (const xmlChar *)"v2");
		indice = strtof((const char*)xmlArray, NULL);
		indices.push_back(indice);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(faces, (const xmlChar *)"v3");
		indice = strtof((const char*)xmlArray, NULL);
		indices.push_back(indice);
		xmlFree(xmlArray);

		if (faces->next->next == NULL)
			break;
		faces = faces->next->next;
	}


	// indexing

	for (unsigned long i = 0; i < indices.size(); ++i)
	{
		long index = indices[i];

		glm::vec3 indexed_vertex = vertices[index];
		glm::vec2 indexed_uv = uvs[index];
		glm::vec3 indexed_normal = normals[index];

		vertOut.push_back(indexed_vertex);
		uvOut.push_back(indexed_uv);
		normalOut.push_back(indexed_normal);
	}

	xmlFreeDoc(document);
	xmlCleanupParser();

	return true;
}
	

/*Implicit cstructor*/
Model::Model():BaseObject(){

	DataInit();
	if (!LoadShaders(GetFullShaderPath("model.vert"), "", GetFullShaderPath("model.frag")))
		std::cout << "Defafult shaders for model were not succesfully compiled" << std::endl;
	GlReqInit();
}


/*Allows to override defalt shaders for model class, needs already full path*/
Model::Model(string vs, string gs, string fs) {

	DataInit();
	if (!LoadShaders(vs, gs, fs))
		std::cout << "Shaders for model were not succesfully compiled" << std::endl;
	GlReqInit();
}
	

/*Protected cstructor, does not reload shaders*/
Model::Model(bool x) {

	DataInit();
}

/*initializes class parameters*/
void Model::DataInit() {

	this->indexWithinScene = -1;
	this->modelPath = "";
	this->texturePath = "";
	this->shadersLoaded = false;
	this->hasTransparency = false;
	this->modelLoaded = false;
	this->textureLoaded = false;

	this->location = glm::vec3(0.f);
	this->rotation = glm::vec3(1.f, 0.f, 0.f);
	this->scale = glm::vec3(1.f);
	this->angle = 0.f;
	this->name = "";
	this->mMatrix = glm::mat4(1.f);
}


/*Setter for its index within scene*/
void Model::SetSceneIndex(int index) {

	this->indexWithinScene = index;
}


/*Setter for its index within scene*/
int Model::GetSceneIndex() {

	return this->indexWithinScene;
}



/*Will initialize opengl state| req things for opengl */
void Model::GlReqInit(){


	/*Generate VAIO*/
	glGenVertexArrays(1, &this->vaoID);	
	/*Use vaio --> Bind VAIO*/
	glBindVertexArray(this->vaoID); 

	/*Generate Texture*/
	glGenTextures(1, &this->texture);


	/*Generate req Buffers*/
	glGenBuffers(1, &this->vertexBuffer);
	glGenBuffers(1, &this->uvBuffer);		
	glGenBuffers(1, &this->normalBuffer);

	SaveGpuBufferHandler(this->vertexBuffer);
	SaveGpuBufferHandler(this->uvBuffer);
	SaveGpuBufferHandler(this->normalBuffer);
}


/*Mtehod buffers data into gpu (using vaio) */
void Model::BufferData(std::vector<glm::vec3>& vertices, std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals){


	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW); //fill buffer with vertices

	glBindBuffer(GL_ARRAY_BUFFER, this->uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW); //fill buffer with uvs

	glBindBuffer(GL_ARRAY_BUFFER, this->normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW); //fill buffer with normals

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo);
	//glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

	//vertices
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//uvs
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	//normals
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_TRUE, 0, 0);

	glBindVertexArray(0); //unbind Vao

	this->verticesCount = vertices.size();
	this->modelLoaded = true;
}


/*Returns model's model matrix*/
glm::mat4 Model::GetModelMatrix() {

	return this->mMatrix;
}


/*Sets state of pipeline*/
void Model::SetPipelineState(){

	glBindVertexArray(this->vaoID);
	if (!this->hasTransparency)
		glEnable(GL_CULL_FACE);
	else glDisable(GL_CULL_FACE);
	
}


/*Renders all objects*/
void Model::Render(glm::mat4& p_mat, glm::mat4& v_mat, glm::vec3 & sun, glm::vec3 & sun_pos, glm::vec3 view_pos){

	if (!this->modelLoaded)
		return;
	
	SetPipelineState();

	if (this->shadersLoaded)
		glUseProgram(this->programID);
	else return;

	if (this->textureLoaded){
		glActiveTexture(GL_TEXTURE0);//use texture unit 0
		glBindTexture(GL_TEXTURE_2D, this->texture); //set it to be generated texture to be TEX2D
		GLuint SamplerID = glGetUniformLocation(this->programID, "samplerOne");
		glUniform1i(SamplerID,0);
	}

	//glBindVertexArray(this->vaoID); // new

	//pass ViewMatrix
	GLuint VmatrixID = glGetUniformLocation(this->programID, "Vmatrix");
	glUniformMatrix4fv(VmatrixID, 1, GL_FALSE, &v_mat[0][0]);

	//pass ProjectionMatrix
	GLuint PmatrixID = glGetUniformLocation(this->programID, "Pmatrix");
	glUniformMatrix4fv(PmatrixID, 1, GL_FALSE, &p_mat[0][0]);

	//pass default light
	GLuint sunID = glGetUniformLocation(this->programID, "Sun");
	glUniform3f(sunID, sun.x, sun.y, sun.z);

	//pass default light pos
	GLuint sunPosID = glGetUniformLocation(this->programID, "SunPos");
	glUniform3f(sunPosID, sun_pos.x, sun_pos.y, sun_pos.z);

	//pass cameraPosition
	GLuint viewPosID = glGetUniformLocation(this->programID, "ViewPos");
	glUniform3f(viewPosID, view_pos.x, view_pos.y, view_pos.z);

	//vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//uvs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, this->uvBuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	//normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, this->normalBuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_TRUE, 0, 0);

	GLuint MmatrixID = glGetUniformLocation(programID, "Mmatrix");
	glUniformMatrix4fv(MmatrixID, 1, GL_FALSE, &this->mMatrix[0][0]);
	glDrawArrays(GL_TRIANGLES, 0, this->verticesCount);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	CleanPipelineState();
}


/*Cleans state of pipeline*/
void Model::CleanPipelineState(){

	glBindVertexArray(0);
}


/*Loads texture*/
bool Model::LoadTexture(string file) {

	if (LoadTextureInternal(file, this->texture)) {
		this->textureLoaded = true;
		this->texturePath = file;
		return true;
	}	
	return false;
}


/*Method loads texture, and buffers it to GPU, with specific texture*/
bool Model::LoadTextureInternal(string file, GLuint texture){

	SDL_Surface* surface;
	int internal_format = GL_RGB; // for bmp//GL_ALPHA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, GL_RGB, GL_RGBA.
	int texel_format = GL_BGR; //for bmp

	surface = IMG_Load(file.c_str());
	if (surface == NULL){
		return false;
	}

	
	glBindTexture(GL_TEXTURE_2D, texture);

	//check if its jpg
	for (size_t i = 0; i< file.size(); ++i) {
		if (file[i] == '.') {
			switch (file[i+1]){
			case 'j': texel_format = GL_RGB;
			default:
				break;
			}
			break;
		}
	}

	//check for png
	if (surface->format->BytesPerPixel == 4){ //then its png
		internal_format = GL_RGBA;
		texel_format = GL_RGBA;
	}

	//else its bmp

	glTexImage2D(
		GL_TEXTURE_2D, //targeted texture
		0, //level for mipmap
		internal_format, //color components in texture
		surface->w, //width
		surface->h, //height
		0, //border, must allways be 0
		texel_format, //format of pixel data,bmp stores B,G,R
		GL_UNSIGNED_BYTE, //data type of the pixels
		surface->pixels //data
	);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);

	SDL_FreeSurface(surface);

	return true;
}


/*Loads SINGLE! model from file (assimp)*/
bool Model::LoadModel(string file, bool has_transparency){

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<int> indices;

	if (!OpenXMLModel(file, vertices, uvs, normals)) {
		return false;
	}

	
	BufferData(vertices, uvs, normals);
	this->modelPath = file;
	return true;
}


/* Will set into object the desire to position itself*/
void Model::SetPosition(glm::vec3 position){

	this->location = position;
}


/* Will set into object the desire to rotate itself*/
void Model::SetRotation( glm::vec3 rotation){

	this->rotation = rotation;
}

/* Will set rotation angle along the rotation axis */
void Model::SetAngle(float angle) {

	this->angle = angle;
}


/* Will set into object the desire to scale itself*/
void Model::SetScale(glm::vec3 scale){

	this->scale = scale;
}


/*Sets addin rotation*/
void Model::SetAddinationalRotation(glm::quat q){

	this->ad_quat = q;
}


glm::vec3 Model::GetPosition(){

	return this->location;
}


glm::vec3 Model::GetRotation(){

	return this->rotation;
}


glm::vec3 Model::GetScale(){

	return this->scale;
}


float Model::GetAngle(){

	return this->angle;
}


glm::quat Model::GetAddinationalRotation() {
	
	return this->ad_quat;
}


/*Will apply pending operations for all models*/
void Model::UpdateMatrix(){

	glm::mat4 newMatrix = glm::mat4(1.f);
	newMatrix = glm::translate(newMatrix, this->location);
	newMatrix = glm::rotate(newMatrix, glm::radians(this->angle), this->rotation);
	newMatrix = glm::scale(newMatrix, this->scale);
	this->mMatrix = newMatrix;
}


/*Saves name for instance*/
void Model::SetName(string s) {

	this->name = s;
}


/*Getter for name*/
string Model::GetName() {

	return this->name;
}


/**Method saves the reference to shader, which is to be used whith this object**/
GLuint Model::BindShaders(GLuint shaders){

	this->programID = shaders;
	this->shadersLoaded = true;
	return this->programID;
}


/*Saves the vbo for destruction in destructor(not vaio, thats saved implicitly)*/
void Model::SaveGpuBufferHandler(GLuint buffer) {

	this->vboList.push_back(buffer);
}


/*Getter for transparency*/
bool Model::HasTransparency() {

	return this->hasTransparency;
}


/*Getter for texturepath*/
string Model::GetTexturePath() {

	return this->texturePath;
}


/*Getter for modelpath*/
string Model::GetModelPath() {

	return this->modelPath;
}


/*Loads vertex, geometry and fragment shaders (use this when Model is dynamically alocated)*/
bool Model::LoadShaders(string v_file, string g_file, string f_dfile) {

	if (!this->shader.LoadShaderProgram(v_file, g_file, f_dfile)) {
		return false;
	}
	BindShaders(this->shader.GetProgram());
	return true;
}



/*Delete all vbos, texture and vaio*/
Model::~Model(){

	for( size_t i = 0; i< this->vboList.size(); ++i){
		glDeleteBuffers(1, &this->vboList[i]);
	}
	glDeleteVertexArrays(1, &this->vaoID);
	glDeleteTextures(1, &this->texture);

}