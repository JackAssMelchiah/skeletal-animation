cmake_minimum_required(VERSION 3.0.0)

#set allowed configurations
set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_CONFIGURATION_TYPES "Debug" CACHE STRING "My multi config types" FORCE)

#set name of project
PROJECT(test)

#if GNU, set flags
IF(CMAKE_COMPILER_IS_GNUCXX)
	message("Compiler is GNUCXX")
	SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -g2")
ENDIF()

#set output run directory
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

#set cmake modul directory
set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules;${CMAKE_MODULE_PATH}")

#define path for resources in program
add_definitions(-DRESOURCE_DIR="${CMAKE_CURRENT_SOURCE_DIR}/Resources/")
#add_definitions(-DRESOURCE_DIR="Resources/")

#define path for shaders in program
add_definitions(-DSHADER_DIR="${CMAKE_CURRENT_SOURCE_DIR}/Shaders/")
#add_definitions(-DSHADER_DIR="Shaders/")


#set CPP files
set(CPP_FILES Skeleton.cpp Animation.cpp Keyframe.cpp Bone.cpp SkeletalModel.cpp Game.cpp Scene.cpp Player.cpp Camera.cpp Shader.cpp BaseObject.cpp Main.cpp Model.cpp Timer.cpp Utils.cpp)

#set HPP files
set(HPP_FILES BoneStructure.hpp Skeleton.hpp Animation.hpp Keyframe.hpp Bone.hpp SkeletalModel.hpp Game.hpp Scene.hpp Player.hpp Camera.hpp Shader.hpp BaseObject.hpp Model.hpp Timer.hpp Utils.hpp)
	
	#set executables
	add_executable(game ${CPP_FILES} ${HPP_FILES}) #future name of builded module and its source
	
	#OPENGL
	find_package(OpenGL REQUIRED)
	if (${OPENGL_FOUND})
		include_directories(${OPENGL_INCLUDE_DIR})
		target_link_libraries(game ${OPENGL_LIBRARIES})
	endif(${OPENGL_FOUND})

	#GLEW
	find_package(GLEW REQUIRED)
	if (${GLEW_FOUND})
		include_directories(${GLEW_INCLUDE_DIRS})
		target_link_libraries(game ${GLEW_LIBRARIES})
	endif (${GLEW_FOUND})

	#SDL
	find_package(SDL2 REQUIRED)
	if (${SDL2_FOUND})
		include_directories(${SDL2_INCLUDE_DIR})
		target_link_libraries(game ${SDL2_LIBRARY})
	endif (${SDL2_FOUND})

	#SDL_IMAGE
	find_package(SDL2_image REQUIRED)
	include_directories(${SDL2_IMAGE_INCLUDE_DIR})
	target_link_libraries(game ${SDL2_IMAGE_LIBRARIES})

	#GLM
	include_directories("${CMAKE_CURRENT_SOURCE_DIR}/lib/headers/glm")
	
	
	#LIBXML2
	find_package(LibXml2 REQUIRED)
	if (${LIBXML2_FOUND})
		include_directories(${LIBXML2_INCLUDE_DIR})
		target_link_libraries(game ${LIBXML2_LIBRARIES})
		add_definitions(${LIBXML2_DEFINITIONS})
	endif (${LIBXML2_FOUND})


set_target_properties(game PROPERTIES LINKER_LANGUAGE CXX)
