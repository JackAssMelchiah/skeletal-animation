#include "BaseObject.hpp"

static int physicsCount = 0;
static int staticCount = 0;

BaseObject::BaseObject(){

	this->utils = Utils::GetInstance();
	this->id = staticCount;
	staticCount ++;
	
}


/*Returns readable string*/
string BaseObject::VectorToString(glm::vec3& v) {

	return "[" + std::to_string(v.x) + ";" + std::to_string(v.y) + ";" + std::to_string(v.z) + "]";
}


/*Returns readable string*/
string BaseObject::VectorToString(glm::vec4& v) {

	return "[" + std::to_string(v.x) + ";" + std::to_string(v.y) + ";" + std::to_string(v.z) + ";" + std::to_string(v.w) + "]";
}


/*Returns readable string*/
string BaseObject::VectorToString(glm::quat& v) {

	return "[" + std::to_string(v.x) + ";" + std::to_string(v.y) + ";" + std::to_string(v.z) + ";" + std::to_string(v.w) + "]";
}


/*Range convertor*/
float BaseObject::ConvertRange(float curr_val, float old_min, float old_max, float new_min, float new_max) {

	return (((curr_val - old_min) * (new_max - new_min)) / (old_max - old_min)) + new_min;

}


/*Returns cstring with full path to resource*/
string BaseObject::GetFullResourcePath(string abs_path) {

	std::string RESOUCE_DIR_PATH = RESOURCE_DIR;
	this->r_paths.push_back(RESOUCE_DIR_PATH + abs_path);
	return this->r_paths[this->r_paths.size() - 1];
}


/*Returns cstring with full path to shader*/
string BaseObject::GetFullShaderPath(string abs_path) {

	std::string SHADER_DIR_PATH = SHADER_DIR;
	this->s_paths.push_back(SHADER_DIR_PATH + abs_path);
	return this->s_paths[this->s_paths.size() - 1];
}


int BaseObject::GetId(){

	return this->id;
}


BaseObject::~BaseObject(){
}


void BaseObject::ResetGlobalTime() {

	utils->ResetGlobalTime();
}


void BaseObject::StartGlobalTime() {

	utils->StartGlobalTime();
}


/*Intended for usage inside main loop*/
float BaseObject::GetFrameTime() {

	return utils->GetFrameTime();
}


float BaseObject::GetTime() {

	return utils->GetTime();
}


void BaseObject::StartFrameTime() {

	utils->StartFrameTime();
}


void BaseObject::StopFrameTime() {

	utils->StopFrameTime();
}



void  BaseObject::PrintLog(std::string s) {

	 utils->Print(s);
}


/*Getter for the current frametime*/
float BaseObject::GetFrameTimeNum() {

	return utils->FrameTime();
}


/*Sets the frametime value which is then read by others, intended for usage in main loop*/
void BaseObject::SetFrameTimeNum(float x) {

	if (x < 0.1) { //protection so debuging wont turn bad( 10fps limit)
		utils->SetFrameTimeNum(x);
	}
}

