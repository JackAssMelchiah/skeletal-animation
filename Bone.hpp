/**********************************
* Bone.hpp
* Holds info about bones
* for more info about class mehods, please check out .cpp file
*/
#ifndef BONE_HPP
#define BONE_HPP

#include "glm.hpp"
#include "gtc/quaternion.hpp"
#include <string>

using namespace std;
class Bone{

public:
	Bone();
	~Bone();

	void SetPosition(glm::vec3);
	void SetRotation(glm::quat);
	void SetScale(glm::vec3);
	void SetId(int);
	void SetParent(int);
	void SetName(string);
	string GetName();
	glm::vec3 GetPosition();
	glm::quat GetRotation();
	glm::vec3 GetScale();
	int GetId();
	int GetParent();


protected:
	
	glm::vec3 position;
	glm::vec3 scale;
	glm::quat rotation;
	int id;
	int parent;
	string name;
};
#endif