#define NO_SDL_GLEXT

/*Visual leak detector*/
//#include <vld.h>


#ifndef RESOURCE_DIR
#define RESOURCE_DIR "Resources/"
#endif

#ifndef SHADER_DIR
#define SHADER_DIR "Shaders/"
#endif

#include <iostream>
#include <vector>
#include <string>

#include <GL/glew.h>//before opengl
#include <SDL.h>
#include <SDL_opengl.h>
#include <glm.hpp>

#include "Model.hpp"
#include "Shader.hpp"
#include "Player.hpp"
#include "Scene.hpp"
#include "Game.hpp"

int SCREEN_WIDTH = 720;//SIRKA
int SCREEN_HEIGHT = 1280;//VYSKA


int main(int, char* args[]);
void close();
bool init();
void clearScreen();
void refresh();
string GetFullResourcePath(string abs_path);
string GetFullShaderPath(string abs_path);
void UpdateFrameTime(BaseObject* object);
void EnableDebug();
std::string translateDebugSource(GLenum source);
std::string translateDebugType(GLenum type);
std::string translateDebugSeverity(GLenum severity);
void defaultDebugMessage(GLenum source, GLenum type, GLuint, GLenum severity, GLsizei, const GLchar *message, void*);

SDL_Window* glWindow = NULL;
SDL_GLContext glContext;

std::vector<string> r_paths; //path to resources
std::vector<string> s_paths; //path to shaders

using namespace std;

int main(int argc, char* args[]) {

	if (!init()) {
		cout << "Failed to initialize!" << endl;
		close();
		return 1;
	}

	

	/*GL parameters setup*/
	glClearColor(1.f, 1.f, 1.f, 1.f);
	glPointSize(10);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	SDL_SetRelativeMouseMode(SDL_TRUE);
	SDL_WarpMouseInWindow(glWindow, SCREEN_HEIGHT / 2, SCREEN_WIDTH / 2);

	/*Class Initialization*/

	//Run the game
	Game game(glWindow, SCREEN_HEIGHT, SCREEN_WIDTH, std::string(RESOURCE_DIR));

	game.Run();
	close();
	
	return 0;
}




bool init() {

	
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		cout << "SDL init err" << endl;
		return false;
	}
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
	//SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);


	//get native resolution of dysplay(1 display )
	SDL_DisplayMode dysplayInfo;
	if (SDL_GetCurrentDisplayMode(0, &dysplayInfo) != 0) {
		std::cout << "Unable to get info about your display" << std::endl;
	}
	else {
		std::cout << "----DysplayInfo----" << std::endl;
		std::cout <<dysplayInfo.h<<"x"<< dysplayInfo.w<<"@"<< dysplayInfo.refresh_rate<< std::endl;
		std::cout << "-------------------" << std::endl;
	//	SCREEN_WIDTH = dysplayInfo.h;
	//	SCREEN_HEIGHT = dysplayInfo.w;
	}


	glWindow = SDL_CreateWindow("Skletal Animation", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_HEIGHT, SCREEN_WIDTH, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN); // | SDL_WINDOW_FULLSCREEN_DESKTOP

	//CONTEXT
	glContext = SDL_GL_CreateContext(glWindow);
	if (glContext == NULL){
		cout << "glcontext err" << endl;
		return false;
	}

	int size;
	SDL_GL_GetAttribute(SDL_GL_DEPTH_SIZE, &size);
	std::cout << "DEPTH BUFFER RESOLUTION IS"<<size << endl;



	//GLEW
	glewExperimental = GL_TRUE;

	GLenum glewError = glewInit();

	if (glewError != GLEW_OK){
		cout << "glew init err" << endl;
		return false;
	}

	if (GLEW_ARB_debug_output) {
		glDebugMessageCallbackARB((GLDEBUGPROCARB)&defaultDebugMessage, nullptr);
	}

	//vsync
	if (SDL_GL_SetSwapInterval(1) < 0){
		cout << "no v-sync" << endl;
	}

	//init SLD IMAGE 
	int flags = IMG_INIT_JPG | IMG_INIT_PNG;
	int init_img = IMG_Init(flags);
	if (init_img & flags != flags){
		cout << "cannot init JPG&PNG! Sorry.";
		return false;
	}

	return true;
}



/*Close*/
void close(){

	SDL_DestroyWindow(glWindow);
	glWindow = NULL;
	IMG_Quit();
	SDL_Quit();
	//getchar();
}


/*Clears color and depth budffer*/
void clearScreen(){

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


/*Swaps buffers of window*/
void refresh() {

	SDL_GL_SwapWindow(glWindow);
}


/*Returns cstring with full path to resource*/
string GetFullResourcePath(string abs_path ) {

	std::string RESOUCE_DIR_PATH = RESOURCE_DIR;
	r_paths.push_back(RESOUCE_DIR_PATH + abs_path);
	return r_paths[r_paths.size()-1];
}


/*Returns cstring with full path to shader*/
string GetFullShaderPath(string abs_path) {

	std::string SHADER_DIR_PATH = SHADER_DIR;
	s_paths.push_back(SHADER_DIR_PATH + abs_path);
	return s_paths[s_paths.size() - 1];
}


/*Updates frame time*/
void UpdateFrameTime(BaseObject* object) {

	object->SetFrameTimeNum(object->GetFrameTime()); //write measured frametime
	object->StopFrameTime();	//clear timer
	object->StartFrameTime();	//start measuring
}


void EnableDebug() {
	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, NULL, GL_FALSE);
	//glDebugMessageControl(GL_DONT_CARE, GL_DEBUG_TYPE_PERFORMANCE, GL_DONT_CARE, 0, NULL, GL_FALSE);
	glDebugMessageCallback((GLDEBUGPROC)(defaultDebugMessage), NULL);
}


void defaultDebugMessage(
	GLenum        source,
	GLenum        type,
	GLuint        /*id*/,
	GLenum        severity,
	GLsizei       /*length*/,
	const GLchar *message,
	void*        /*userParam*/) {
	std::cout <<
		"source: " << translateDebugSource(source) <<
		" type: " << translateDebugType(type) <<
		" severity: " << translateDebugSeverity(severity) <<
		" : " << message << std::endl;
}



std::string translateDebugSource(GLenum source) {
	switch (source) {//swich over debug sources
	case GL_DEBUG_SOURCE_API:return"GL_DEBUG_SOURCE_API";
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:return"GL_DEBUG_SOURCE_WINDOW_SYSTEM";
	case GL_DEBUG_SOURCE_SHADER_COMPILER:return"GL_DEBUG_SOURCE_SHADER_COMPILER";
	case GL_DEBUG_SOURCE_THIRD_PARTY:return"GL_DEBUG_SOURCE_THIRD_PARTY";
	case GL_DEBUG_SOURCE_APPLICATION:return"GL_DEBUG_SOURCE_APPLICATION";
	case GL_DEBUG_SOURCE_OTHER:return"GL_DEBUG_SOURCE_OTHER";
	default:return"unknown";
	}
}


std::string translateDebugType(GLenum type) {
	switch (type) {//switch over debug types
	case GL_DEBUG_TYPE_ERROR:return"GL_DEBUG_TYPE_ERROR";
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:return"GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR";
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:return"GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR";
	case GL_DEBUG_TYPE_PORTABILITY:return"GL_DEBUG_TYPE_PORTABILITY";
	case GL_DEBUG_TYPE_PERFORMANCE:return"GL_DEBUG_TYPE_PERFORMANCE";
	case GL_DEBUG_TYPE_MARKER:return"GL_DEBUG_TYPE_MARKER";
	case GL_DEBUG_TYPE_PUSH_GROUP:return"GL_DEBUG_TYPE_PUSH_GROUP";
	case GL_DEBUG_TYPE_POP_GROUP:return"GL_DEBUG_TYPE_POP_GROUP";
	case GL_DEBUG_TYPE_OTHER:return"GL_DEBUG_TYPE_OTHER";
	case GL_DONT_CARE:return"GL_DONT_CARE";
	default:return"unknown";
	}
}


std::string translateDebugSeverity(GLenum severity) {
	switch (severity) {//switch over debug severities
	case GL_DEBUG_SEVERITY_LOW:return"GL_DEBUG_SEVERITY_LOW";
	case GL_DEBUG_SEVERITY_MEDIUM:return"GL_DEBUG_SEVERITY_MEDIUM";
	case GL_DEBUG_SEVERITY_HIGH:return"GL_DEBUG_SEVERITY_HIGH";
	case GL_DEBUG_SEVERITY_NOTIFICATION:return"GL_DEBUG_SEVERITY_NOTIFICATION";
	case GL_DONT_CARE:return"GL_DONT_CARE";
	default:return"unknown";
	}
}