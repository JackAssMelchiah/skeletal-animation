/********************************** 
* BaseObject.hpp
* Base object class is base class (uses singleton pattern),
* which provides an unique IDs for each object in game scene,
* as well as interface for getting few things such as unified frametime
* for more info about class mehods, please check out .cpp file
*/

#ifndef BASEOBJECT_HPP
#define BASEOBJECT_HPP

#include "Utils.hpp"
#include <string>
#include "gtc/quaternion.hpp"

class BaseObject{

public:

	BaseObject();
	~BaseObject();
	int GetId();
	void ResetGlobalTime();
	void StartGlobalTime();
	float GetFrameTime();
	float GetTime();
	void StartFrameTime();
	void StopFrameTime();
	void PrintLog(std::string);
	float GetFrameTimeNum();
	void SetFrameTimeNum(float x);
	string GetFullResourcePath(string abs_path);
	string GetFullShaderPath(string abs_path);
	float ConvertRange(float curr_val, float old_min, float old_max, float new_min, float new_max);
	static string VectorToString(glm::vec3& v);
	static string VectorToString(glm::vec4& v);
	static string VectorToString(glm::quat& v);


	
	
private:
	int id;
	Utils *utils;
	std::vector<string>r_paths;
	std::vector<string>s_paths;
};


#endif

