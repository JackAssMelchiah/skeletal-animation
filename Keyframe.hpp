#ifndef KEYFRAME_HPP
#define KEYFRAME_HPP

#include <vector>
#include "glm.hpp"

#include "BoneStructure.hpp"

class Keyframe{

public:
	Keyframe( BoneStructure* b, float time);
	BoneStructure* GetBoneStructure();
	float GetKeyFrameTime();
	void SetKeyFrameTime(float);
	std::vector<glm::mat4>* GetPose();
	~Keyframe();
	
private:
	BoneStructure* Bones;
	float time;
	std::vector<glm::mat4> globalPose; //might cause problems
};

#endif