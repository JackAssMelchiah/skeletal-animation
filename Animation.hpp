#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include <vector>
#include "glm.hpp"

#include "Keyframe.hpp"

class Animation{

public:
	Animation();
	~Animation();
	void SetName(string);
	void SetLength(float seconds);
	void SetLoopable(bool);
	string GetName();
	float GetLength();
	bool IsLooping();
	Keyframe* GetKeyFrame(int );
	int KeyFrameCount();
	void AddKeyFrame(BoneStructure* bone, float time);
	void ModifyAnimationSpeed(float newspeed);
	float GetAnimationSpeed();

protected:

	std::vector<Keyframe*> AnimationKeyframes;
	float length;
	float speed;
	bool loopable;
	string name;
	
};

#endif