/**********************************
* Utils.hpp
* Serves mainly as logging "service"
**for more info about class mehods, please check out .cpp file
*/

#ifndef UTILS_HPP
#define UTILS_HPP

#include <time.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <glm.hpp>
#include <gtc/quaternion.hpp>
#include "Timer.hpp"

using namespace std;
class Utils{

public:
	static Utils* GetInstance();
	Utils(Utils const&) = delete;            // Don't Implement
	Utils* operator=(Utils const&) ;
	float GetTime();						 //MILISECONDS
	float GetFrameTime();					 //MILISECONDS
	void StartGlobalTime();
	void ResetGlobalTime();
	void StartFrameTime();
	void StopFrameTime();
	void Print(string s);
	void SetFrameTimeNum(float x);
	float FrameTime();
	static std::string VecToStr(glm::vec3);
	static glm::vec3 Interpolate(glm::vec3& A, glm::vec3& B, float t);
	static glm::quat Interpolate(glm::quat& A, glm::quat& B, float t);

private:
	Utils();
	~Utils();
	void newFile();

	/*Timers initialization*/
	Timer time; // global timer
	Timer deltaTime; // time from the last frame
	float TIME;
	ofstream myfile;
	bool fileOpened;
	float _frameTime;

};



#endif 


