#include "Shader.hpp"


Shader::Shader(){

	this->loaded_shader = false;
}


Shader::~Shader(){

	if (this->loaded_shader) {
		glDeleteProgram(this->programID);
	}
}


/**Method loads vertex and fragment shaders, and compiles them into program**/
bool Shader::LoadShaderProgram(string vertexShaderPath, string geometryShaderPath, string fragmentShaderPath){

	bool returnval = true;
	GLint result = GL_FALSE;
	GLuint vertexID;
	GLuint geometryID;
	GLuint fragmentID;

	if (vertexShaderPath == "")
		return false;

	vertexID = LoadShaders(vertexShaderPath, VERTEX_SHADER, &returnval);
	if (!returnval) {
		return false;
	}

	if (geometryShaderPath != "") {
		geometryID = LoadShaders(geometryShaderPath, GEOMETRY_SHADER, &returnval);
		if (!returnval) {
			return false;
		}
	}
	if (fragmentShaderPath != "") {
		fragmentID = LoadShaders(fragmentShaderPath, FRAGMENT_SHADER, &returnval);
		if (!returnval) {
			return false;
		}
	}

	this->programID = glCreateProgram();
	glAttachShader(this->programID, vertexID);
	if (geometryShaderPath != "") {
		glAttachShader(this->programID, geometryID);
	}
	if (fragmentShaderPath != "") {
		glAttachShader(this->programID, fragmentID);
	}
	
	glLinkProgram(this->programID);

	// Check the program
	glGetProgramiv(this->programID, GL_LINK_STATUS, &result);
	if (result == GL_FALSE){
		PrintProgramLog(this->programID);
		returnval = false;
	}

	glDeleteShader(vertexID);
	if (geometryShaderPath != "")
		glDeleteShader(geometryID);
	if (fragmentShaderPath != "")
		glDeleteShader(fragmentID);


	return returnval;
}


/*Method compiles shaders(VERTEX || FRAGMENT), returns handler to them*/ //POSSIBLE STATIC
GLuint Shader::LoadShaders(string path, int type , bool *was_ok){

	GLuint shaderID;
	// Create the shaders
	if (type == VERTEX_SHADER){
		shaderID = glCreateShader(GL_VERTEX_SHADER);
	}

	else if (type == GEOMETRY_SHADER){
		shaderID = glCreateShader(GL_GEOMETRY_SHADER);
	}

	else if (type == FRAGMENT_SHADER){
		shaderID = glCreateShader(GL_FRAGMENT_SHADER);
	}

	std::string src;
	
	if (!ShaderFromFile(path, src)){
		std::cout << "Incorrect path to shader!" <<std::endl;
		*was_ok = false;
		return 0;
	}

	GLchar* source = ((GLchar *)src.c_str());//const


	GLint Result = GL_FALSE;

	glShaderSource(shaderID, 1, &source, NULL);
	glCompileShader(shaderID);

	// Check shader
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &Result);
	if (Result == GL_FALSE){
		PrintfShaderLog(shaderID);
	}


	*was_ok = true;
	this->loaded_shader = true;
	return shaderID;
}


/**Method gets the program log, and prints it**///POSSIBLE STATIC
void Shader::PrintfShaderLog(GLuint shader){

	if (glIsShader(shader)){
		int infoLogLength = 0;
		int maxLength = infoLogLength;

		//Get info string length
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

		//Allocate string
		char* infoLog = new char[ maxLength ];

		//Get info log
		glGetProgramInfoLog(shader, maxLength, &infoLogLength, infoLog);
		if (infoLogLength > 0){
			std::cout << infoLog << std::endl;
		}
	}
}


/*Method gets the shader log, and prints it*/ //POSSIBLE STATIC
void Shader::PrintProgramLog(GLuint program){

	if (!glIsProgram(program)){
		return;
	}

	int infoLogLength = 0;
	int maxLength = infoLogLength;

	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

	char* infoLog = new char[ maxLength ];

	glGetProgramInfoLog(program, maxLength, &infoLogLength, infoLog);
	if (infoLogLength > 0){
		std::cout << infoLog << std::endl;
	}	
}


/*Method opens the file, and returns NULL terminated array with all text in it */ //POSSIBLE STATIC
bool Shader::ShaderFromFile(string shaderPath, string & shaderHolder){

	FILE* f;
	f = fopen(shaderPath.c_str(), "r");
	if (f == NULL){
		return false;
	}

	int c;

	while (c = fgetc(f)){
		if (c == EOF)
			break;
		shaderHolder += c ;
	}


	fclose(f);
	return true;
}


GLuint Shader::GetProgram() {

	return this->programID;
}