#version 150 core

in vec3 fragNormal;
in vec2 uv;
in vec3 dirToLight;
in vec3 dirToSun;

out vec4 color;

uniform sampler2D samplerOne; 


uniform vec3 Light;
uniform vec3 Sun;
uniform vec3 ViewPos;

vec3 calcDiffuse(in vec3 fNormal, in vec3 dirLight, in vec3 lightCol, in vec3 attenuation);

void main(){
	

	vec4 textureColor = texture(samplerOne, uv).rgba;
	if (textureColor.a < 0.1){
		discard;
	}

//totals
	vec3 totalDiffuse = vec3(0.f);
	vec3 attenuation =  vec3(1.f,0.f,0.f);

	//calculate difuse of sun
	totalDiffuse+= calcDiffuse(fragNormal, dirToSun, Sun, attenuation);

	//result
	//color = vec4(totalDiffuse, 1.f) * textureColor;
	color = textureColor;
	

}


// diffuse Calculation
vec3 calcDiffuse(in vec3 fNormal, in vec3 dirLight, in vec3 lightCol, in vec3 attenuation){

	vec3 normNormal = normalize(fNormal);
	float distance = length(dirLight);
	float atFac = attenuation.x + attenuation.y * distance + attenuation.z * distance * distance;
	vec3 normDirToLight = normalize(dirLight);
	float diff = max(dot(normNormal, normDirToLight),0.4);
	return (diff * lightCol)/atFac;
}

		
 
