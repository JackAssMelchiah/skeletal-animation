#version 150 core
#extension GL_ARB_explicit_attrib_location : require

#define MAXJOINTS 50

layout(location = 0) in vec4 vertex;
layout(location = 2) in vec3 color_bones;

out vec3 color_bone;

uniform mat4 Mmatrix;
uniform mat4 Vmatrix;
uniform mat4 Pmatrix;
uniform mat4 Joints[MAXJOINTS];

void main(){

	int index = int(vertex.w);
	color_bone = color_bones;
	gl_Position = Pmatrix * Vmatrix * Joints[index] * vec4(vertex.xyz, 1);	
}
