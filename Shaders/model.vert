#version 150 core
#extension GL_ARB_explicit_attrib_location : require

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 vertexUV; 
layout(location = 2) in vec3 normal;


out vec2 uv; 
out vec3 fragNormal;
out vec3 dirToLight;
out vec3 dirToSun;

uniform mat4 Mmatrix;
uniform mat4 Vmatrix;
uniform mat4 Pmatrix;

uniform vec3 LightPosition;
uniform vec3 SunPos;


void main(){

	vec4 WorldPos = Mmatrix * vec4(vertex, 1); //vertex in world position
	uv = vertexUV;	//pass directly
	dirToLight = LightPosition - WorldPos.xyz; //for when applying scene trans
	dirToSun =  SunPos - WorldPos.xyz;
	fragNormal = normalize((Mmatrix * vec4(normal, 0)).xyz); //aply model transf to normals
	gl_Position = Pmatrix * Vmatrix * WorldPos;	
}
