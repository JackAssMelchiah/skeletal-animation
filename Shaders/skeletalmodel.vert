#version 150 core
#extension GL_ARB_explicit_attrib_location : require

#define MAXJOINTS 50

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec2 vertexUV; 
layout(location = 2) in vec3 normal;
layout(location = 3) in vec4 weights;
layout(location = 4) in ivec4 ids;


out vec2 uv; 
out vec3 fragNormal;
out vec3 dirToLight;
out vec3 dirToSun;

uniform mat4 Mmatrix;
uniform mat4 Vmatrix;
uniform mat4 Pmatrix;

uniform vec3 LightPosition;
uniform vec3 SunPos;

uniform mat4 Joints[MAXJOINTS];
uniform mat4 InvBPose[MAXJOINTS];


void main(){

	mat4 skinned_volume = (Joints[ids.x] * InvBPose[ids.x]) * weights.x;
	skinned_volume += (Joints[ids.y] * InvBPose[ids.y]) * weights.y;
	skinned_volume += (Joints[ids.z] * InvBPose[ids.z]) * weights.z;
	skinned_volume += (Joints[ids.w] * InvBPose[ids.w]) * weights.w;

	vec4 WorldPos = Mmatrix * skinned_volume * vec4(vertex, 1); //vertex in world position
	uv = vertexUV;	//pass directly
	dirToLight = LightPosition - WorldPos.xyz; //for when applying scene trans
	dirToSun =  SunPos - WorldPos.xyz;
	fragNormal = normalize((Mmatrix * vec4(normal, 0)).xyz); //aply model transf to normals
	gl_Position = Pmatrix * Vmatrix * WorldPos;	
}
