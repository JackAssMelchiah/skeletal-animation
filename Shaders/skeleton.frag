#version 150 core

in vec3 color_bone;
out vec4 color;

void main(){
	
	color = vec4(color_bone, 1.f);
}

		
 
