#include "Keyframe.hpp"
#include <iostream>

/*Inicializes how the bones look like in keyframe at time*/
Keyframe::Keyframe(BoneStructure* b, float time){

	this->Bones = b;
	this->time = time;
}


/*Return all keyframe's bones */
BoneStructure* Keyframe::GetBoneStructure() {

	return this->Bones;
}


/*Returns time at which the keyframe should be interpolated to 100%*/
float Keyframe::GetKeyFrameTime() {

	return this->time;
}


/*Returns time at which the keyframe should be interpolated to 100%*/
void Keyframe::SetKeyFrameTime( float t) {

	this->time = t;
}


/*Returns pose, in which are saved joint matrices in global coords*/
std::vector<glm::mat4>* Keyframe::GetPose(){

	return &this->globalPose;
}


/*frees all bones, then frees bone structure*/
Keyframe::~Keyframe(){

	for(auto& i: *this->Bones){
		delete i.second;
		//this->Bones->erase(i.first); -> would invalidate iterator
	}
	this->Bones->clear();
	delete this->Bones;	
}