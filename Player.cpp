#include "Player.hpp"

Player::Player(SDL_Window* w){
	
	this->boundCamera = NULL;
	int width;
	int height;
	SDL_GetWindowSize(w, &width, &height);
	this->mode = CamMode::FPS;
	Camera *c = new Camera(width, height);
	BindCamera(c);
	this->mouseCoords = glm::vec2(0.f);
	this->wheel = 1;
}


/*Get mode of camera bound to player*/
Player::CamMode Player::GetMode(){

	return this->mode;
}


/*Input method to invoke calculating motion on keyboard, the mouse motion executed in updateInputs*/
void Player::CalculateMotion() {


	if (this->mode == CamMode::FPS) { //update according to 1ps
		MoveCam1PSKeyboard(); // move acc to keyboard
	}
}



/*Updates the cam pos and rotation*/
void Player::MoveCam1PSKeyboard() {

	float speed = 1.0;

	/*W*/
	if (this->keyboard[SDLK_w]) {
		this->boundCamera->GoForward(speed);
	}
	/*S*/
	if (this->keyboard[SDLK_s]) {
		this->boundCamera->GoBackward(speed);
	}
	/*A*/
	if (this->keyboard[SDLK_a]) {
		this->boundCamera->StrafeLeft(speed);
	}
	/*D*/
	if (this->keyboard[SDLK_d]) {
		this->boundCamera->StrafeRight(speed);
	}
	/*X*/
	if (this->keyboard[SDLK_LCTRL]) {
		this->boundCamera->GoDown(speed);
	}
	/*C*/
	if (this->keyboard[SDLK_SPACE]) {
		this->boundCamera->GoUp(speed);
	}
}


/*Updates the cam pos and rotation*/
void Player::MoveCam1PSMouse() { 

	this->boundCamera->MouseUpdate(this->mouseCoords);
}


/*Updates the model pos and rotation*/
/*if th movement and rotation of vehicle are disabled, then cam is rot. around vehicle*/


/*Pools inputs*/
bool Player::UpdateInputs() {

	while (SDL_PollEvent(&e) != 0) {
		/*User requests quit*/
		if (e.type == SDL_QUIT) {
			return false;
		}
		
		if (e.type == SDL_KEYDOWN){
			this->keyboard[e.key.keysym.sym] = true;		//KEY_DOWN
		}

		if (e.type == SDL_KEYUP) {
			this->keyboard[e.key.keysym.sym] = false;		//KEY_DOWN
		}

		/*Handle mouse*/	
		if (e.type == SDL_MOUSEMOTION) {

			int x, y;
			Uint8 mouseState = SDL_GetRelativeMouseState(&x, &y);
			if (this->mode == CamMode::FP){
				this->mouseCoords.x += 0.1f;
				this->mouseCoords.y += 0.1f;
			}

			
			else if (this->mode == CamMode::FPS) {
				this->mouseCoords.x = (float)x*0.2f;
				this->mouseCoords.y = (float)y*0.2f;
				MoveCam1PSMouse();
			}
			
		}

		if (e.type == SDL_MOUSEWHEEL){

			this->wheel += e.wheel.y;

		}


		if (e.type == SDL_WINDOWEVENT) {
			if (e.window.event == SDL_WINDOWEVENT_RESIZED) {
				int win_data_1 = e.window.data1;
				int win_data_2 = e.window.data2;
				std::cout << "Window resized to" << win_data_1 << "x" << win_data_2 << endl;
				//glViewport(0, 0, win_data_1, win_data_2);
				this->boundCamera->SetnewWindowParameters(win_data_2, win_data_1); //projection
				//need to update viewport and projection matrix
			}
		}
	}
	return true;
}


/*Returns move values for fixed point camera*/
glm::vec3 Player::getMove(){

	return glm::vec3(this->mouseCoords.x,this->mouseCoords.y, -40.f);
}


/* Returns scale vector accordingly*/
glm::vec3 Player::GetScaleFactor(){

	glm::vec3 retVec = glm::vec3(1.f) + glm::vec3(this->wheel/10.f);
	std::cout << retVec.x << retVec.y << retVec.z <<std::endl;
	return retVec;
}


/*Getter for bound camera*/
Camera* Player::GetBoundCamera() {
		
	return this->boundCamera;
}


/*Binds camera if there is no camera bound!*/
bool Player::BindCamera(Camera *cam) {

	if (this->boundCamera != NULL)
		return false;

	this->boundCamera = cam;
	return true;
}


/*Getter for mouse coordinates*/
glm::vec2 Player::GetMouseCoords() {

	return this->mouseCoords;
}


/*Removes reference to camera from Player(still exists in memory)*/
void Player::RemoveCamera() {

	this->boundCamera = NULL;
}


Player::~Player() {
	
	delete this->boundCamera;
}
