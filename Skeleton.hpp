#ifndef SKELETON_HPP
#define SKELETON_HPP

#include <memory>

#include "Model.hpp"
#include "BoneStructure.hpp"
#include "Animation.hpp"
#include "gtc/quaternion.hpp"

#include <libxml/parser.h>
#include <libxml/tree.h>

class Bone;

class Skeleton: public Model{


public:
	Skeleton();
	Skeleton(string vs, string fs);
	~Skeleton();

	void GlReqInit();
	void Render(glm::mat4& p_mat, glm::mat4& v_mat);
	void UpdateMatrix();
	bool LoadModel(string file);
	bool LoadTexture(string file);
	static void PrintBones(BoneStructure* b, string s);
	void StartAnimation(Animation* animation);
	void EndAnimation();
	string GetAnimName(int index);
	int GetAnimationCount();
	Animation* GetAnimation( int index);
	std::vector<glm::mat4>* GetActiveJoints();
	std::vector<glm::mat4>* GetInverseJoints();

protected:
	void BufferData(std::vector<glm::vec4>& vertices, std::vector<glm::vec3>& color);
	void SetPipelineState();
	void CleanPipelineState();
	bool LoadTextureInternal(string file, GLuint texture);
	bool CreateInverseBPose();
	bool CreatePose(Keyframe*);
	void GetSkeletonVertices(std::vector<glm::vec4>&);
	void FillVerticesColors(std::vector<glm::vec3>&);
	bool CreateGlobalPose(BoneStructure* source, std::vector<glm::mat4>* target);
	
private:
	void Animate(Animation* selected_animation);
	void DataInit();
	bool OpenSkeletonOGRE(string file);
	void LoadCurrentAnimation(xmlNode* current_node, Animation* animation, std::map<std::string, int>& tTable);
	void LoadAllKeyframesOfSingleBone(xmlNode* current_node, Animation* animation, int bone_id);
	BoneStructure* CreateDeepCopyOfBoneStructure();
	void FillKeyFrame(xmlNode* current_node, Animation* animation, float k_f_time, int bone_id, int keyframe_index);
	int GetFutureKeyFrameCount(xmlNode* keyframes);
	glm::mat4 CreateJointMatrix(Bone*);
	void GetAndFillInterpolatedFrame(BoneStructure* k1, BoneStructure* k2, float percent);
	void CreateInterpolatedBonesStructure();
	/*attributes*/

	Animation* renederedAnimation;
	BoneStructure Bones;
	std::map<int, Animation*> Animations;
	std::vector<glm::mat4> InvBindPose;
	std::vector<glm::mat4> BindPose;

	std::vector<glm::mat4> InterpolatedJoints;
	BoneStructure* InterpolatedJointsBs;

	Timer animationTimer;
	int nextKeyFrameHeuristics;
	

};

#endif