/**********************************
* Camera.hpp
* Class represents camera within scene
* for more info about class mehods, please check out .cpp file
*/

#ifndef CAMERA_HPP
#define CAMERA_HPP


#define NO_SDL_GLEXT
#define GLM_FORCE_RADIANS

#include <glm.hpp>
#include "gtc/matrix_transform.hpp"
#include "BaseObject.hpp"
#include "gtx/rotate_vector.hpp"


class Camera: public BaseObject{

public:
	Camera( int width, int height);
	glm::mat4 GetWorldToViewMatrix();
	glm::mat4 GetViewToProjection();
	glm::vec3 GetCamPos();
	void SetCamPos(glm::vec3);
	void SetCamViewDir(glm::vec3);
	void MouseUpdate(const glm::vec2& newMouseCoords);
	void GoForward(float acceleration = 0.1f);
	void GoBackward(float acceleration = 0.1f);
	void StrafeRight(float acceleration = 0.1f);
	void StrafeLeft(float acceleration = 0.1f);
	void GoUp(float acceleration = 0.1f);
	void GoDown(float acceleration = 0.1f);
	void SetMode(int);
	void SetPitch(float);
	void SetRoll(float);
	int GetHeight();
	int GetWidth();
	void SetnewWindowParameters(int width, int height);
	glm::vec2 rtsStartLocation;
private:
	glm::vec3 position;
	glm::vec3 viewDirection; //always normalized
	glm::vec3 up;
	glm::vec2 oldMouseCoords;
	int width;
	int height;
	int mode;
	float pitch;
	
	
	
};


#endif

