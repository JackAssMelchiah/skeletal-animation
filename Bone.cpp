#include "Bone.hpp"

Bone::Bone() {

	this->position = glm::vec3(0.f);
	this->rotation = glm::quat();
	this->scale = glm::vec3(1.f);
	this->id = -1;
	this->parent = -2;
	this->name = "";
}


void Bone::SetPosition(glm::vec3 position) {
	
	this->position = position;
}


void Bone::SetRotation(glm::quat rotation) {

	this->rotation = rotation;
}


void Bone::SetScale(glm::vec3 scale) {

	this->scale = scale;
}


void Bone::SetId(int id) {

	this->id = id;
}


void Bone::SetParent(int parent) {

	this->parent = parent;
}


void Bone::SetName(string name) {

	this->name = name;
}


string Bone::GetName() {

	return this->name;
}


glm::vec3 Bone::GetPosition() {

	return this->position;
}


glm::quat Bone::GetRotation() {

	return this->rotation;
}


glm::vec3 Bone::GetScale() {

	return this->scale;
}


int Bone::GetId(){

	return this->id;
}


int Bone::GetParent() {

	return this->parent;
}



Bone::~Bone(){


}