/**********************************
* SkeletalModel.hpp
* Class model holds info about skeltal model used in scene(such as vertices, ..., textures, colision bodies,..) 
* for more info about class mehods, please check out .cpp file
*/

#ifndef SKELETAL_MODEL_HPP
#define SKELETAL_MODEL_HPP




#include "Model.hpp"
#include "Skeleton.hpp"

class Animation; 
class Bone;

class SkeletalModel: public Model{

	enum class DrawMode { MODEL, SKELETON, MODEL_SKELETON };
public:
	SkeletalModel();
	SkeletalModel(string vs, string gs, string fs, string svs, string sfs);
	void GlReqInit();
	void Render(glm::mat4& p_mat, glm::mat4& v_mat, glm::vec3 & sun, glm::vec3 & sun_pos, glm::vec3 view_pos);
	void UpdateMatrix();
	bool LoadModel(string file, bool has_transparency);
	bool LoadTexture(string file);
	~SkeletalModel();
	Skeleton* GetSkeleton();
protected:

	/*methods*/
	void BufferData(std::vector<glm::vec3>& vertices, std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals, std::vector<glm::vec4>& weights, std::vector<glm::ivec4>& boneIds);
	void SetPipelineState();
	void CleanPipelineState();
	bool LoadTextureInternal(string file, GLuint texture);


private:
	void DataInit();
	bool OpenModel(string filename, std::vector<glm::vec3>& vertOut, std::vector<glm::vec2>& uvOut, std::vector<glm::vec3>& normalOut, std::vector<glm::vec4>& weightsOut, std::vector<glm::ivec4>& bonesIdsOut, string& skeletonFileNameOut);
	
	Skeleton* skeleton;
	GLuint weightsBuffer;
	GLuint bonesIDBuffer;
	
	glm::ivec4 weights;
	glm::ivec4 weightsIds;

	DrawMode drawMode;

};
#endif