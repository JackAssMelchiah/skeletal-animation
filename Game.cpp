#include "Game.hpp"



Game::Game( SDL_Window* window, int screen_width, int screen_height, std::string resource_path) {

	this->mainWindow = window;
	this->gameScene = NULL;
	this->activeScene = NULL;

	this->screenHeight = screen_height;
	this->screenWidth = screen_width;
	this->resourcePath = resource_path;
}


/*Creates new game scene*/
bool Game::CreateGameScene() {

	
	Player *p = new Player(this->mainWindow);
	Scene *s = new Scene( p, this->mainWindow); // this->bd
	//here load and add models to scene

	this->mainPlayer = p;

	SkeletalModel* m = new SkeletalModel();


	m->LoadModel(m->GetFullResourcePath("Cylinder.001.mesh.xml"), false);
	m->LoadTexture(m->GetFullResourcePath("texture.bmp"));
	s->AddModel(m);

	Animation *a = m->GetSkeleton()->GetAnimation(0);
	//a->ModifyAnimationSpeed(0.5f);
	a->SetLoopable(true);
	m->GetSkeleton()->StartAnimation(a);

		
	this->gameScene = s;
	s->StartGlobalTime();
	return true;
}


/*Sets the game scene to be rendered instead of menu scene*/
void Game::SetToGame() {

	this->activeScene = this->gameScene;
	//s->StartGlobalTime();
}


/*Destroys scene in memory*/
void Game::FreeScene(Scene* s) {

	if (s == NULL) {
		return;
	}

	delete s;
	s = NULL;
}



/*Run the game, entry point*/
void Game::Run() {

	//draw the loading scene
	CreateGameScene();
	SetToGame();
	MainLoop();
	
}


/*Main game loop*/
void Game::MainLoop() {


	bool dontExit = true;
	while (dontExit) {
		ClearScreen();		
		
		dontExit = this->activeScene->DrawScene();
		//do swap logic here
		Refresh();
		UpdateSceneFrameTime(this->activeScene);
	
	}
	//edning now, so free player and camera
	DestroyPlayer(this->mainPlayer);
	FreeScene(this->activeScene);
}


/*Destroys main player, called before game exits*/
void Game::DestroyPlayer(Player *p) {

	delete p;
}


/*Clears color and depth budffer*/
void Game::ClearScreen() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}


/*Swaps buffers*/
void Game::Refresh() {

	SDL_GL_SwapWindow(this->mainWindow);
}


/*Updates frame time to accuratelly reflect last frame refresh*/
void Game::UpdateSceneFrameTime(Scene* s) {

	s->SetFrameTimeNum(s->GetFrameTime()); //write measured frametime
	s->StopFrameTime();	//clear timer
	s->StartFrameTime();	//start measuring
}


Game::~Game() {
	
	
}
