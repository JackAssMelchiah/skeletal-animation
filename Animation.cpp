#include "Animation.hpp"


Animation::Animation(){

	this->length = 0.f;
	this->loopable = false;
	this->name = "";
	this->speed = 1.f;
}


/*Sets name of animation*/
void Animation::SetName(string n) {

	this->name = n;
}


/*Set Length for animation in seconds, use only in animation loader.
* For rescaling animation length, use ModifyAnimationSpeed();
*/
void Animation::SetLength(float seconds) {

	this->length = seconds;
}


/*Speeds up or slows down animation*/
void Animation::ModifyAnimationSpeed(float newspeed) {

	this->speed = newspeed;
}


/*Gets total speed of animation(in percents)*/
float Animation::GetAnimationSpeed() {

	return this->speed;
}



/*Set if aniamtion should loop*/
void Animation::SetLoopable(bool b) {

	this->loopable = b;
}


/*Returns name of animation*/
string Animation::GetName() {

	return this->name;
}


/*Returns full length of animation in seconds*/
float Animation::GetLength() {

	return this->length;
}


/*Returns whatever animation loops */
bool Animation::IsLooping() {
	
	return this->loopable;
}


/*Gets single keyframe within Animation::KeyFrameCount() or NULL*/
Keyframe* Animation::GetKeyFrame(int index) {
	
	if (index < this->AnimationKeyframes.size())
		return this->AnimationKeyframes[index];
	return NULL;
}


/*Returns count of keyframes the animation has*/
int Animation::KeyFrameCount() {

	return this->AnimationKeyframes.size();
}


/*Adds new keyframe*/
void Animation::AddKeyFrame(BoneStructure* bone, float time) {

	if (bone != NULL)
		this->AnimationKeyframes.push_back(new Keyframe(bone, time));
}


Animation::~Animation(){

	//delete keyframes
	for (int i = 0; i < this->AnimationKeyframes.size(); ++i) {
		delete AnimationKeyframes[i];
	}
}