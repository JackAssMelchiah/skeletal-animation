#include "Timer.hpp"

Timer::Timer()
{
	started = false;
	paused = false;
	startTicks = 0;
	pausedTicks = 0;
}

void Timer::Start()
{
	started = true;
	paused = false;
	startTicks = SDL_GetTicks();
	pausedTicks = 0;
}

void Timer::Stop()
{
	started = false;
	paused = false;
	startTicks = 0;
	pausedTicks = 0;
}


void Timer::Pause()
{
	if (!paused && started)
	{
		paused = true;
		pausedTicks = SDL_GetTicks() - startTicks; //get current time
		startTicks = 0;
	}
}

void Timer::Unpause()
{
	if (paused && started)
	{
		paused = false;
		startTicks = SDL_GetTicks() - pausedTicks;
		pausedTicks = 0; //get current time
	}
}

Uint32 Timer::GetTicks()
{
	if (started)
	{
		if (paused)
		{
			return pausedTicks;
		}
		else
		{
			return SDL_GetTicks() - startTicks;
		}
	}
	return 0;
}

bool Timer::IsStarted()
{
	return started;
}

bool Timer::IsPaused()
{
	return paused;
}

Timer::~Timer()
{
}

