#include "Skeleton.hpp"

/*Default parameters*/
Skeleton::Skeleton():Model(true) {

	DataInit();
	if (!LoadShaders(GetFullShaderPath("skeleton.vert"), "", GetFullShaderPath("skeleton.frag")))
		std::cout << "Defafult shaders for skeleton were not succesfully compiled" << std::endl;
	GlReqInit();
}


/*Allows for different shaders for skeleton*/
Skeleton::Skeleton(string vs, string fs):Model(true) {
		
	DataInit();
	if (!LoadShaders(vs, "", fs))
		std::cout << "Shaders for skeleton were not succesfully compiled" << std::endl;
	GlReqInit();
}


/*Init important class data*/
void Skeleton::DataInit() {

	this->InterpolatedJointsBs = NULL;
	this->renederedAnimation = NULL;
	this->nextKeyFrameHeuristics = 1;
	
}


/*GL atributes init*/
void Skeleton::GlReqInit() {

	Model::GlReqInit();
}


/*Render Skeleton*/
void Skeleton::Render(glm::mat4& p_mat, glm::mat4& v_mat) {

	if (!this->modelLoaded)
		return;

	SetPipelineState();

	if (this->shadersLoaded)
		glUseProgram(this->programID);
	else return;

	//pass ViewMatrix
	GLuint VmatrixID = glGetUniformLocation(this->programID, "Vmatrix");
	glUniformMatrix4fv(VmatrixID, 1, GL_FALSE, &v_mat[0][0]);

	//pass ProjectionMatrix
	GLuint PmatrixID = glGetUniformLocation(this->programID, "Pmatrix");
	glUniformMatrix4fv(PmatrixID, 1, GL_FALSE, &p_mat[0][0]);

	//vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	//colors
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, this->normalBuffer);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_TRUE, 0, 0);

	//model matrix //redundant
	GLuint MmatrixID = glGetUniformLocation(this->programID, "Mmatrix");
	glUniformMatrix4fv(MmatrixID, 1, GL_FALSE, &this->mMatrix[0][0]);

	//joints
	GLuint JointsID = glGetUniformLocation(this->programID, "Joints");
	glUniformMatrix4fv(JointsID, this->Bones.size() ,GL_FALSE, &this->InterpolatedJoints[0][0][0]);
	
	glDrawArrays(GL_LINES, 0, this->verticesCount); //this->verticesCount

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(2);
	CleanPipelineState();

}


/*Starts animation, if loopable, then it will loop*/
void Skeleton::StartAnimation(Animation* animation) {

	this->renederedAnimation = animation;
	this->animationTimer.Start();

}


/*Prematurelly Ends animation*/
void Skeleton::EndAnimation() {

	this->nextKeyFrameHeuristics = 1;
	this->animationTimer.Stop();

	if (this->renederedAnimation->IsLooping()) {
		this->animationTimer.Start();
		return;
	}

	this->renederedAnimation = NULL;
}


/*Animates whole mesh*/
void Skeleton::Animate(Animation* selected_animation) {

	//gets 2 keyframes according to time their time, inerpolates between them, WARNING!! ANIMATION MUST CONTAIN ATLEAST 2 KEYFRAMES
	if (selected_animation == NULL) {
		GetAndFillInterpolatedFrame(this->Animations.begin()->second->GetKeyFrame(0)->GetBoneStructure(), this->Animations.begin()->second->GetKeyFrame(1)->GetBoneStructure(), 0.f);
		return; //tpose
	}
	//animate
	int keyframe_count = selected_animation->KeyFrameCount();

	float current_time = this->animationTimer.GetTicks() / 1000.f;
	float frame_time = this->GetFrameTimeNum();
	float animation_speed = selected_animation->GetAnimationSpeed();

	//modify animation speed by frametime and animation_speed
	current_time = current_time * animation_speed;

	//std::cout << "T::"<< frame_time << std::endl;

	if (current_time  >= selected_animation->GetLength()) { //its over, cancel animation
		EndAnimation();
		return;
	}

	while (selected_animation->GetKeyFrame(this->nextKeyFrameHeuristics)->GetKeyFrameTime() < current_time ) {
		this->nextKeyFrameHeuristics++;
	}

	float k0_t = selected_animation->GetKeyFrame(this->nextKeyFrameHeuristics - 1)->GetKeyFrameTime();
	float k1_t = selected_animation->GetKeyFrame(this->nextKeyFrameHeuristics)->GetKeyFrameTime();
	float t  = current_time - k0_t; //ts

	float percent = 100.f / ((k1_t - k0_t) / t) / 100.f; //%

	GetAndFillInterpolatedFrame(selected_animation->GetKeyFrame(this->nextKeyFrameHeuristics - 1)->GetBoneStructure(), selected_animation->GetKeyFrame(this->nextKeyFrameHeuristics)->GetBoneStructure(), percent);
}


/*Returns name of animation*/
string Skeleton::GetAnimName(int index) {

	if (index < this->Animations.size()) {
		return this->Animations[index]->GetName();
	}
	return string("**ERR**");
}


/*Returns animation count*/
int Skeleton::GetAnimationCount() {

	return this->Animations.size();
}


/*Returns animation, saved at index*/
Animation* Skeleton::GetAnimation(int index) {

	if (index < this->Animations.size()) {
		return this->Animations[index];
	}
	return NULL;
}


/*Updates modelmatrix as well as computes*/
void Skeleton::UpdateMatrix() {

	Model::UpdateMatrix();
	//update animation
	Animate(this->renederedAnimation);
}


/*Loads Skeleton file, fills up bones, animations and returns in param vertices for skeleton render*/
bool Skeleton::OpenSkeletonOGRE(string file) {
	  //ZPřEHLEDNIT
	xmlDoc* document;

	xmlNode* root_element;
	xmlNode* bone_node;
	xmlNode* current_node;
	xmlNode* animation_node;

	int bone_count = 0;
	Bone* bone = NULL;

	xmlChar* xmlArray;

	int id = -1;
	string name;
	glm::vec3 position;
	glm::vec3 rotation;
	float angle;

	std::map<std::string, int> tTable;

	document = xmlReadFile(file.c_str(), "UTF-8", 0); //| XML_PARSE_RECOVER
	if (document == NULL) {
		std::cout << "Provided path is not a valid XML" << std::endl;
		xmlCleanupParser();
		return false;
	}

	root_element = xmlDocGetRootElement(document);
	if (root_element == NULL) {
		std::cout << "Provided path is not a valid XML" << std::endl;
		xmlFreeDoc(document);
		xmlCleanupParser();
	}


	bone_node = root_element->children->next->children->next; // get on bone
	current_node = bone_node;
	++bone_count; //already on first bone
				 //get Count of geometry models
	do {
		if (!xmlStrcmp(current_node->name, (const xmlChar*) "bone")) {
			++bone_count;
		}
		current_node = current_node->next->next;
	} while (current_node->next->next != NULL);


	current_node = bone_node;


	while (1) {
		//new bone, get id and name
		xmlArray = xmlGetProp(current_node, (const xmlChar *)"id");
		id = strtol((const char*)xmlArray, NULL, 10);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(current_node, (const xmlChar *)"name");
		name = (const char*)xmlArray;
		xmlFree(xmlArray);

		//get the position
		current_node = current_node->children->next; //1 deeper
													 //printf("%s\n",current_node->name); 
		xmlArray = xmlGetProp(current_node, (const xmlChar *)"x");
		position.x = strtof((const char*)xmlArray, NULL);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(current_node, (const xmlChar *)"y");
		position.y = strtof((const char*)xmlArray, NULL);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(current_node, (const xmlChar *)"z");
		position.z = strtof((const char*)xmlArray, NULL);
		xmlFree(xmlArray);

		//get rotation angle
		current_node = current_node->next->next;
		
		xmlArray = xmlGetProp(current_node, (const xmlChar *)"angle");
		angle = strtof((const char*)xmlArray, NULL);
		xmlFree(xmlArray);

		//get the rotation axis, 1 deeper
		current_node = current_node->children->next;

		xmlArray = xmlGetProp(current_node, (const xmlChar *)"x");
		rotation.x = strtof((const char*)xmlArray, NULL);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(current_node, (const xmlChar *)"y");
		rotation.y = strtof((const char*)xmlArray, NULL);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(current_node, (const xmlChar *)"z");
		rotation.z = strtof((const char*)xmlArray, NULL);
		xmlFree(xmlArray);

		bone = new Bone();
		bone->SetId(id);
		bone->SetName(name);
		bone->SetPosition(position);
		bone->SetRotation(glm::angleAxis(angle, rotation));
		
		//now add bone to  skeleton
		this->Bones.insert(std::pair<int, Bone*>(bone->GetId(), bone));

		//now add it to trans.table
		tTable.insert(std::pair<std::string, int>(name, id));

		bone = NULL;
		//now get back to next bone
		if (bone_node->next->next == NULL)
			break;

		bone_node = bone_node->next->next;
		current_node = bone_node;
	}

	//create links between bones, reuse bone node and current node

	bone_node = root_element->children->next->next->next;
	current_node = bone_node->children->next;

	while (1) {
		xmlArray = xmlGetProp(current_node, (const xmlChar *)"parent"); //pick bones parent 
		string xmlArrayParent = (const char*)xmlArray;
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(current_node, (const xmlChar *)"bone"); //pick its child
		string xmlArrayNode = (const char*)xmlArray;
		xmlFree(xmlArray);

		this->Bones[tTable[xmlArrayNode]]->SetParent(tTable[xmlArrayParent]);

		if (current_node->next->next == NULL) {
			break;
		}
		current_node = current_node->next->next;
	}
	//parent of root bode is implicitly -2


#ifdef DEBUG
	PrintBones(&this->Bones, "DEFAULT BONES");
#endif // DEBUG


	//load animations
	current_node = root_element->children->next; // bones
	while (1) {
		if (!xmlStrcmp((const xmlChar *)"animations", current_node->name)) {
			//found animations, there still doesnt have to be anything
			if (current_node->children->next == NULL) {
				break;
			}

			animation_node = current_node->children->next;
			int anim_index = 0;
			Animation* animation = NULL;
			//loop thru animations
			while (1) {
				//load animation
				animation = new Animation();
				LoadCurrentAnimation(animation_node, animation, tTable);
				//save loaded animation
				this->Animations.insert(std::pair<int, Animation*>(anim_index, animation));
				anim_index++;

				if (animation_node->next->next == NULL)
					break;
				animation_node = animation_node->next->next;
			}
		}
		if (current_node->next->next == NULL)
			break;
		current_node = current_node->next->next;
	}	
	xmlCleanupParser();
	xmlFreeDoc(document);
	return true;
}


void Skeleton::PrintBones(BoneStructure* b, string s) {
	/* std::cout << "*********" << s << "**************" << std::endl << std::endl;
	 for (BoneStructureIterator i = b->begin(); i != b->end(); ++i) {
	 	std::cout << "**************" << std::endl;
	 	std::cout << "NAME:" << i->second->GetName();
	 	std::cout << ", ID:" << i->second->GetId();
	 	std::cout << ", Parent:" << i->second->GetParent() << std::endl;
	 	std::cout << "\t Position:" << VectorToString(i->second->GetPosition()) << std::endl;
	 	std::cout << "\t Rotaion:" << VectorToString(i->second->GetRotation()) << std::endl;
	 	std::cout << "\t Scale:" << VectorToString(i->second->GetScale()) << std::endl;
	 	std::cout << "**************" << std::endl;
	 }*/
}


/*Creates deep copy, except for pos rot scale, from loaded bonestructure in skeleton, returns pointer to it*/
BoneStructure* Skeleton::CreateDeepCopyOfBoneStructure() {

	BoneStructure* new_bones = new BoneStructure();
	Bone *b;

	BoneStructureIterator it;
	for (it = this->Bones.begin(); it != this->Bones.end(); ++it) {
		b = new Bone();
		b->SetId(this->Bones[it->first]->GetId());
		b->SetName(this->Bones[it->first]->GetName());
		b->SetParent(this->Bones[it->first]->GetParent());
		
		b->SetPosition(this->Bones[it->first]->GetPosition());
		b->SetRotation(this->Bones[it->first]->GetRotation());
		b->SetScale(this->Bones[it->first]->GetScale());
		
		new_bones->insert(std::pair<int, Bone*>(b->GetId(), b));
	}

	return new_bones;
}


/*Returns count of keyframes, needs xmlNode to be pointing att <keyframes>*/
int Skeleton::GetFutureKeyFrameCount( xmlNode* keyframes) {

	int count = 0;
	keyframes = keyframes->children->next;
	while (1) {
		++count;
		if (keyframes->next->next != NULL)
			keyframes = keyframes->next->next;
		else
			break;
	}
	return count;
}


/*Loads single animation from XML skeleton file, returns pointer to filled animation */ //TODO:: this seems to load only first keyframe, need to make LoadCurrentKeyFrame()
void Skeleton::LoadCurrentAnimation(xmlNode* current_node, Animation* animation, std::map<std::string, int>& tTable) {

	xmlChar* xmlArray;

	xmlArray = xmlGetProp(current_node, (const xmlChar *)"name");
	animation->SetName((const char*) xmlArray);
	xmlFree(xmlArray);


	xmlArray = xmlGetProp(current_node, (const xmlChar *)"length");
	animation->SetLength(strtof((const char*)xmlArray, NULL));
	xmlFree(xmlArray);


	current_node = current_node->children->next; //<tracks>
	current_node = current_node->children->next; //<track>

	//get num of keyframes of the animation
	int keyframe_count = GetFutureKeyFrameCount(current_node->children->next);	//there must be atleast one keyframe, otherwise would be anim wihout keyframe --- no sense

	//create keyframe_count of keyframes
	for (int i = 0; i < keyframe_count; ++i) {
		animation->AddKeyFrame(CreateDeepCopyOfBoneStructure(), -1.f);
	}

	int iteration = 0;
	while (1) {	//loop thru all <track name = "bone">(all bones), fill each keyframes for single bone  
		
		xmlArray = xmlGetProp(current_node, (const xmlChar *)"bone"); //get name
		LoadAllKeyframesOfSingleBone(current_node, animation, tTable[(const char*)xmlArray]);
		xmlFree(xmlArray);
		if (current_node->next->next == NULL)
			break;
		current_node = current_node->next->next;
	} 
}


/*Loads all keyframes of single bone and saves them to animation*/
void Skeleton::LoadAllKeyframesOfSingleBone(xmlNode* current_node, Animation* animation, int bone_id ) {

	int keyframe_count = animation->KeyFrameCount();

	float k_f_time = -1.f;
	int keyframe_in_file_count = 0;
	xmlChar* xmlArray;
	int keyframe_fill_index = 0;	//used for keyframe index in fillng
	
	//now at <track name = "bone">
	current_node = current_node->children->next;
	//now at <keyframes> -->this must be here 
	current_node = current_node->children->next;
	//now at <keyframe time ="0.0"> -->this must be here atleast once, or there would be no keyframes 

	while (1) {

		xmlArray = xmlGetProp(current_node, (const xmlChar *)"time"); //get name
		k_f_time = strtof((const char*)xmlArray, NULL);
		xmlFree(xmlArray);
		//fill keyframe for every bone keyframes 
		FillKeyFrame(current_node, animation, k_f_time, bone_id, keyframe_fill_index);
		++keyframe_fill_index;
	
		//looping within keyframes
		if (current_node->next->next != NULL)
			current_node = current_node->next->next;
		else break;
	}
}


/*Interpolates Joints into Interpolated joints, precent needs to be clamped to 0-1*/
void Skeleton::GetAndFillInterpolatedFrame(BoneStructure* k1, BoneStructure* k2, float percent) {
	
	//fills interpolated bones wtih bindpose + interpolated value of k1 a and k2
	BoneStructureIterator it;
	glm::quat r1;
	glm::quat r2;
	glm::vec3 p1;
	glm::vec3 p2;
	glm::vec3 s1;
	glm::vec3 s2;

	for (it = this->Bones.begin(); it != this->Bones.end(); ++it) {

		r1 = k1->at(it->first)->GetRotation();
		r2 = k2->at(it->first)->GetRotation();

		p1 = k1->at(it->first)->GetPosition();
		p2 = k2->at(it->first)->GetPosition();

		s1 = k1->at(it->first)->GetScale();
		s2 = k2->at(it->first)->GetScale();
		
		this->InterpolatedJointsBs->at(it->first)->SetPosition(this->Bones[it->first]->GetPosition() + Utils::Interpolate(p1, p2, percent));
		this->InterpolatedJointsBs->at(it->first)->SetRotation(this->Bones[it->first]->GetRotation() *  Utils::Interpolate(r1, r2, percent));
		this->InterpolatedJointsBs->at(it->first)->SetScale(Utils::Interpolate(s1, s2, percent));
	}
	this->InterpolatedJoints.clear();
	//now create matrices from the interpolated position
	CreateGlobalPose(this->InterpolatedJointsBs, &this->InterpolatedJoints);
}


/*Creates holder for "keyframe", which is allways drawn*/
void Skeleton::CreateInterpolatedBonesStructure() {

	this->InterpolatedJointsBs = CreateDeepCopyOfBoneStructure();
}


/*Fills each keyframe's within animation one bone, which is given by bone_id */
void Skeleton::FillKeyFrame(xmlNode* current_node, Animation* animation, float k_f_time, int bone_id, int keyframe_index) {

	Bone* b;
	xmlChar* xmlAdinArray;
	glm::vec3 position;
	glm::vec3 axis;
	glm::vec3 scale;
	float angle;
	
	animation->GetKeyFrame(keyframe_index)->SetKeyFrameTime(k_f_time);
	b = animation->GetKeyFrame(keyframe_index)->GetBoneStructure()->at(bone_id);

	//now search for vals and ubdate b
	//lets pretend that all values are allways specified

	//<position>
	xmlAdinArray = xmlGetProp(current_node->children->next, (const xmlChar *)"x");
	position.x = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	xmlAdinArray = xmlGetProp(current_node->children->next, (const xmlChar *)"y");
	position.y = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	xmlAdinArray = xmlGetProp(current_node->children->next, (const xmlChar *)"z");
	position.z = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	//<angle>
	xmlAdinArray = xmlGetProp(current_node->children->next->next->next, (const xmlChar *)"angle");
	angle = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	//<rotation axis>
	xmlAdinArray = xmlGetProp(current_node->children->next->next->next->children->next, (const xmlChar *)"x");
	axis.x = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	xmlAdinArray = xmlGetProp(current_node->children->next->next->next->children->next, (const xmlChar *)"y");
	axis.y = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	xmlAdinArray = xmlGetProp(current_node->children->next->next->next->children->next, (const xmlChar *)"z");
	axis.z = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	//<scale>
	xmlAdinArray = xmlGetProp(current_node->children->next->next->next->next->next, (const xmlChar *)"x");
	scale.x = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	xmlAdinArray = xmlGetProp(current_node->children->next->next->next->next->next, (const xmlChar *)"y");
	scale.y = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);
	xmlAdinArray = xmlGetProp(current_node->children->next->next->next->next->next, (const xmlChar *)"z");
	scale.z = strtof((const char*)xmlAdinArray, NULL);
	xmlFree(xmlAdinArray);

	b->SetPosition(position);
	b->SetScale(scale);
	b->SetRotation(glm::angleAxis(angle, axis));
	
}


/*Loads the skeleton file of OGRE file*/
bool Skeleton::LoadModel(string file) {

	std::vector<glm::vec4> b_vertices;
	std::vector<glm::vec3> colors;

	//load joints, animations, keyframes
	if (!OpenSkeletonOGRE(file)) {
		return false;
	}
	//Creates structure for interpolated keyframe, this keyframe is the one that will be rendered
	CreateInterpolatedBonesStructure();
	//create inverse joints in global space for tpose
	CreateInverseBPose();
	//create joints in global space for tpose
	CreatePose(NULL);
	CreatePose(this->Animations.begin()->second->GetKeyFrame(1)); //create first keyframe of animation 0
	//create joints in global space for each animation --> well only if we are not gona blend animations at runtime and animation keyframes are already interpolated, then its useful to precalculate them
	//for (int i = 0; i < this->Animations.size(); ++i) {
	//	for (int j = 0; j < this->Animations[i]->KeyFrameCount(); ++j) {
	//		CreatePose(this->Animations[i]->GetKeyFrame(j));
	//	}
	//	//prostorova slozitost moc vysoka, lepsi to delat dynamicky
	//}

	//fills vector with vertices which represents bones
	GetSkeletonVertices(b_vertices);
	//fills colors
	FillVerticesColors(colors);
	//buffer it
	BufferData(b_vertices, colors);
	//all is well
	this->modelPath = file;
	return true;
}


/*Transaltes source bones from local joint coords to global, stored in target, needs to be empty*/
bool Skeleton::CreateGlobalPose( BoneStructure* source, std::vector<glm::mat4>* target) {

	if (target->size() != 0 || source->size() == 0)
		return false;
	glm::mat4 temp;
	BoneStructureIterator b_i;
	int j;
	for (b_i = source->begin(); b_i != source->end(); ++b_i) {
		j = b_i->first;
		temp = glm::mat4(1.f);
		while (j != -2) {
			temp = CreateJointMatrix(source->at(j)) * temp;
			j = source->at(j)->GetParent();		//new bone to process
		}
		target->push_back(temp);
	}
	return true;
}


/*Creates and saves to specific array inverse tpose*/
bool Skeleton::CreateInverseBPose() {

	if (!CreateGlobalPose(&this->Bones, &this->InvBindPose))
		return false;

	for (int i = 0; i < this->InvBindPose.size(); ++i) {
		this->InvBindPose[i] = glm::inverse(this->InvBindPose[i]);
	}
	return true;
}


/*Creates and saves specific pose(means screates requred matrix hierarchy) from specific keyframes, or if NULL, from Tpose*/
bool Skeleton::CreatePose(Keyframe* keyframe) {

	BoneStructure* source;
	std::vector<glm::mat4>* target;

	if (keyframe == NULL) {			//this one just for the funs
		source = &this->Bones;
		target = &this->BindPose;
	}
	else {
		source = keyframe->GetBoneStructure();
		target = keyframe->GetPose();
	}

	return CreateGlobalPose(source, target);
}


/*Creates joint matrix, and returns it*/
glm::mat4 Skeleton::CreateJointMatrix(Bone* b) {

	glm::mat4 temp = glm::mat4(1.f);
	temp = glm::translate(temp, b->GetPosition());
	temp = temp * glm::toMat4(b->GetRotation());
	temp = glm::scale(temp, b->GetScale());
	return temp;
}


/*Will return filled up vertices which represents bones, with w component as id of bone */
void Skeleton::GetSkeletonVertices( std::vector<glm::vec4>& vertices) {

	//fill vertices 
	for (int i = 0; i < this->Bones.size(); ++i) {
		vertices.push_back(glm::vec4(0.f, 0.f, 0.f, 1.f));
		vertices.push_back(glm::vec4(0.f, 0.3f, 0.f, 1.f)); 
	}

	//sets ids for vertices
	for (int i = 0; i < this->Bones.size(); ++i) {
		vertices[2 * i].w = float(this->Bones[i]->GetId());		//id is as float now, need to cast it to int in shader later on
		vertices[2 * i + 1].w = float(this->Bones[i]->GetId());	//id is as float now, need to cast it to int in shader later on
	}
}


/*Fills color vector for bones*/
void Skeleton::FillVerticesColors(std::vector<glm::vec3>& colors) {

	for (int i = 0; i < this->Bones.size() * 2; ++i) {
		colors.push_back(glm::vec3(1.f, 0.f, 1.f));
	}
}


/*Not implemented*/
bool Skeleton::LoadTexture(string file) {

	return false;
}


/*Buffers data, uses normalbuffer as color (2), doesnt use uvbuffer(1)*/
void Skeleton::BufferData(std::vector<glm::vec4>& vertices, std::vector<glm::vec3>& color) {

	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec4), &vertices[0], GL_STATIC_DRAW); //fill buffer with vertices

	glBindBuffer(GL_ARRAY_BUFFER, this->normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, color.size() * sizeof(glm::vec3), &color[0], GL_STATIC_DRAW); //fill buffer with normals

	 //vertices																								
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, 0);

	//colors
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_TRUE, 0, 0);

	glBindVertexArray(0); //unbind Vao

	this->verticesCount = vertices.size();
	this->modelLoaded = true;
}


/*Sets pipeline global state*/
void Skeleton::SetPipelineState() {

	glBindVertexArray(this->vaoID);
	glDepthFunc(GL_ALWAYS);
	glLineWidth(5.f);
}


/*Resets pipeline to default state*/
void Skeleton::CleanPipelineState() {

	glBindVertexArray(0);
	glDepthFunc(GL_LESS);
	glLineWidth(1.f);
}


/*Not implemented*/
bool Skeleton::LoadTextureInternal(string file, GLuint texture) {

	return false;
}


/*Getter for active joints which should be rendered*/
std::vector<glm::mat4>* Skeleton::GetActiveJoints() {

	return &this->InterpolatedJoints;
}


/*Getter for inverse tpose joints*/
std::vector<glm::mat4>* Skeleton::GetInverseJoints() {

	return &this->InvBindPose;
}


/*Destructor*/
Skeleton::~Skeleton(){

	this->renederedAnimation = NULL; //this one is deleted wth other animations

	//delete InterpolatedJointsBs
	BoneStructureIterator b_i;
	for (b_i = this->InterpolatedJointsBs->begin(); b_i != this->InterpolatedJointsBs->end(); ++b_i) {
		delete b_i->second;
	}
	this->InterpolatedJointsBs->clear();
	delete(this->InterpolatedJointsBs);

	//delete Animations
	std::map<int, Animation*>::iterator a_i;
	for (a_i = this->Animations.begin(); a_i != this->Animations.end(); ++a_i) {
		delete a_i->second;
	}
	this->Animations.clear();

	//delete bones
	for (auto i : this->Bones) {
		delete i.second;
	}
	this->Bones.clear();

}
