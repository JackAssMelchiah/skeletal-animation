#include "Utils.hpp"


Utils::Utils(){

	this->fileOpened = false;
	_frameTime = 0.016f; //set first frame to be 60fps
}
  

Utils::~Utils(){

	if (this->fileOpened) {
		myfile.close();
	}
}


 Utils* Utils::GetInstance() {

	 static Utils instance;

	 return &instance;

}


 Utils * Utils::operator=(Utils const &){

	 return this->GetInstance();
 }


 float Utils::GetTime() {

	this->TIME += float(this->time.GetTicks() / 1000.f);
	 if (this->TIME > 1) {
		 this->time.Stop();
		 this->time.Start();
		 if (this->TIME > 3600) { //hour is gone, reset to prevent overflow
			 this->TIME -= 3600;
			 //printf("timer resset\n");
		 }
	 }
	 return this->TIME;
 }


float Utils::GetFrameTime() {

	return float(this->deltaTime.GetTicks() / 1000.f);
	
}


void Utils::StartGlobalTime() {

	this->time.Start();

}


void Utils::ResetGlobalTime() {

	this->time.Stop();
	this->TIME = 0.f;
	this->time.Start();
}


void Utils::StartFrameTime() {

	this->deltaTime.Start();
}


void Utils::StopFrameTime() {

	this->deltaTime.Stop();
}


void Utils::Print(string s) {

	if (!this->fileOpened)
		newFile();

	this->myfile << s.c_str() << endl;	
}


void Utils::newFile() {

	
	this->myfile.open("log.txt");
	this->fileOpened = true;

}


void Utils::SetFrameTimeNum(float x) {

	this->_frameTime = x;
}


float Utils::FrameTime() {

	return this->_frameTime;
}


std::string Utils::VecToStr(glm::vec3 v) {

	string s = "[" + std::to_string(v.x) + ", " + std::to_string(v.y) +", "+ std::to_string(v.z) + "]";
	return s;
}


/*vec3 LERP*/
glm::vec3 Utils::Interpolate(glm::vec3& A, glm::vec3& B, float t) {

	return B*t + A*(1.f - t);
}


/* Quat Slerp*/
glm::quat Utils::Interpolate(glm::quat& A, glm::quat& B, float t) {

	return glm::slerp(A, B, t);
}
