/**********************************
* Player.hpp
* Class extends GameLogic, represents controlable player.
* Player has vehicle, few particles, race model and camera
* Also Player checks for inputs from keyboard/dance pad
* for more info about class mehods, please check out .cpp file
*/




#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "Camera.hpp"

#include <map>

#define CAM_MODE_PASSIVE 0
#define CAM_MODE_1PS 1



class Player{

	

public:
	enum class CamMode{FPS, RTS, FP};

	Player(SDL_Window* w);
	~Player();

	void CalculateMotion();
	bool BindCamera(Camera *cam);
	Camera* GetBoundCamera();
	void RemoveCamera();
	glm::vec2 GetMouseCoords();
	bool UpdateInputs();
	Player::CamMode GetMode();
	glm::vec3 GetScaleFactor();
	glm::vec3 getMove();

private:	
	void MoveCam1PSKeyboard();
	void MoveCam1PSMouse();
	
	//InOut vars
	SDL_Event e;
	std::map<int, bool> keyboard; //keyboard
	glm::vec2 mouseCoords;
	
	//bounded objects
	Camera *boundCamera;
	CamMode mode;
	glm::vec3 moveVec;
	int wheel;
	
};

#endif 
