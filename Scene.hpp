/**********************************
* Scene.hpp
* Class incorporates game logic for flying rocket, and its model and particles
* Scene renders each object bound within it
* Within context of the scene, there are callbacks for bullet collision
* Object which stores information(such as visibility) about objects, groups them together, allowing only parts of groups to be rendered
**for more info about class mehods, please check out .cpp file
*/


#ifndef SCENE_HPP
#define SCENE_HPP


#include <vector>
#include <iostream>
#include <map>

#include <ostream>

#include "BaseObject.hpp"
#include "Model.hpp"
#include "Camera.hpp"
#include "Shader.hpp"
#include "Player.hpp"


/*Scene expects every bound object to be dynamically allocated, since it will dealocate them when scene is destroyed when calling destroy | object is destroyed */
class Scene : public BaseObject {


public:
	
	Scene(Player* p, SDL_Window* window);
	virtual bool DrawScene();
	
	void AddPlayer(Player* p);
	int AddModel(Model* m);
	int AddCamera(Camera *);
	
	bool RemoveModel(int);
	bool RemoveCamera(int);
	void RemovePlayer();
	
	bool DestroyModel(int);
	bool DestroyCamera(int);	
	~Scene();

protected:
	SDL_Window *window;
	
	std::map<int, Model*> sceneModels;  // ivec.x -> index of model(default 0), ivec.y -> visibility in scene  //acces sceneModels[i].first and .second;
	std::map<int, Camera*> sceneCameras;
	
private:
	Player* mainPlayer;
	int activeCameraIndex;
	glm::vec3 globalLight;
	glm::vec3 globalLightPos;
};
#endif

