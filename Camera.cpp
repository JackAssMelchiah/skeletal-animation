#include "Camera.hpp"
#include <iostream>


/*Sets Position of camera, where to look, right and up vector, field of view, display ratio and min-max range of displayed objects  */
Camera::Camera(int width, int height) : viewDirection(0.f,0.f,1.f), up(0.f,1.f,0.f){

	this->width = height;
	this->height = width;
	this->position = glm::vec3(0.f, 5.f, -25.f);
	this->oldMouseCoords = glm::vec2(0.f, 0.f);
	this->pitch = 0.f;
}


/*Re-sets the winndow parameters*/
void Camera::SetnewWindowParameters(int width, int height) {
	
	this->width = width;
	this->height = height;
}


/*Return width*/
int Camera::GetWidth() {

	return this->width;
}


/*Return height*/
int Camera::GetHeight() {

	return this->height;
}


glm::mat4 Camera::GetWorldToViewMatrix(){

	return glm::lookAt(position, position+ glm::normalize(viewDirection), up);
}

glm::mat4 Camera::GetViewToProjection(){

	return glm::perspective(45.f, (float)height / width, 0.1f, 10000.f);
}

glm::vec3 Camera::GetCamPos(){
	
	return position;
}

void Camera::SetCamPos(glm::vec3 pos) {

	position = pos;
}

void Camera::SetCamViewDir(glm::vec3 dir) {

	viewDirection = dir;
}


void Camera::MouseUpdate(const glm::vec2& newMouseCoords) {

	float acceleration = 0.8f;
	glm::vec2 mouseDelta = newMouseCoords + oldMouseCoords;
	mouseDelta.x = mouseDelta.x * acceleration * this->GetFrameTimeNum(); 
	mouseDelta.y = mouseDelta.y * acceleration * this->GetFrameTimeNum();
	//x
	viewDirection = glm::mat3(glm::rotate(-mouseDelta.x, up)) * viewDirection;
	//y
	viewDirection = glm::mat3(glm::rotate(mouseDelta.y, glm::cross(up, viewDirection))) * viewDirection;
	oldMouseCoords = newMouseCoords;
}


void Camera::SetPitch(float angle) {

	viewDirection = glm::mat3(glm::rotate(glm::radians(angle), glm::cross(up, viewDirection))) * viewDirection;
}


void Camera::SetRoll(float angle) {

	viewDirection = glm::mat3(glm::rotate(glm::radians(angle), up)) * viewDirection;
}


void Camera::GoForward(float acceleration){
	position += acceleration * glm::normalize(viewDirection); 
}

void Camera::GoBackward(float acceleration){
	position -= acceleration * glm::normalize(viewDirection);
}

void Camera::StrafeRight(float acceleration){
	position += acceleration * glm::cross(glm::normalize(viewDirection),up);
}

void Camera::StrafeLeft(float acceleration){
	position -= acceleration * glm::cross(glm::normalize(viewDirection), up);
}

void Camera::GoUp(float acceleration){
	position += acceleration *  up;
}

void Camera::GoDown(float acceleration){
	position -= acceleration *  up;
}

void Camera::SetMode(int mode) {

	this->mode = mode;
}
