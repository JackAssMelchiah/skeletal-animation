#ifndef  BONE_STRUCT
#define BONE_STRUCT

#include <map>
#include "Bone.hpp"

typedef std::map<int, Bone*> BoneStructure;
typedef std::map<int, Bone*>::iterator BoneStructureIterator;

#endif