#include "SkeletalModel.hpp"

/*Default cstructor*/
SkeletalModel::SkeletalModel():Model(true){

	DataInit();
	if (!LoadShaders(GetFullShaderPath("skeletalmodel.vert"), "", GetFullShaderPath("skeletalmodel.frag")))
		std::cout << "Defafult shaders for SkeletalModel were not succesfully compiled" << std::endl;

	GlReqInit();
	this->skeleton = new Skeleton();
}


/*Allows for different shaders for skeletal model class and skeleton*/
SkeletalModel::SkeletalModel(string vs, string gs, string fs, string svs, string sfs):Model(true) {

	DataInit();
	if (!LoadShaders(vs, gs, fs))
		std::cout << "Shaders for SkeletalModel were not succesfully compiled" << std::endl;

	GlReqInit();
	this->skeleton = new Skeleton(svs, sfs);
}


/*Init class information*/
void SkeletalModel::DataInit() {

	
	this->drawMode = DrawMode::MODEL_SKELETON;
	this->modelLoaded = false;
}


void SkeletalModel::GlReqInit(){
	
	Model::GlReqInit();
	/*Generate req Buffers*/
	glGenBuffers(1, &this->weightsBuffer);
	glGenBuffers(1, &this->bonesIDBuffer);

	SaveGpuBufferHandler(this->weightsBuffer);
	SaveGpuBufferHandler(this->bonesIDBuffer);
}


/*renders model and|or skeleton*/
void SkeletalModel::Render(glm::mat4& p_mat, glm::mat4& v_mat, glm::vec3 & sun, glm::vec3 & sun_pos, glm::vec3 view_pos){

	
	if (!this->modelLoaded)
		return;

	if (this->drawMode == DrawMode::MODEL || this->drawMode == DrawMode::MODEL_SKELETON) {
		SetPipelineState();

		if (this->shadersLoaded)
			glUseProgram(this->programID);
		else return;

		if (this->textureLoaded) {
			glActiveTexture(GL_TEXTURE0);//use texture unit 0
			glBindTexture(GL_TEXTURE_2D, this->texture); //set it to be generated texture to be TEX2D
			GLuint SamplerID = glGetUniformLocation(this->programID, "samplerOne");
			glUniform1i(SamplerID, 0);
		}

		glBindVertexArray(this->vaoID);

		
		//pass ViewMatrix
		GLuint VmatrixID = glGetUniformLocation(this->programID, "Vmatrix");
		glUniformMatrix4fv(VmatrixID, 1, GL_FALSE, &v_mat[0][0]);

		//pass ProjectionMatrix
		GLuint PmatrixID = glGetUniformLocation(this->programID, "Pmatrix");
		glUniformMatrix4fv(PmatrixID, 1, GL_FALSE, &p_mat[0][0]);

		//pass default light
		GLuint sunID = glGetUniformLocation(this->programID, "Sun");
		glUniform3f(sunID, sun.x, sun.y, sun.z);

		//pass default light pos
		GLuint sunPosID = glGetUniformLocation(this->programID, "SunPos");
		glUniform3f(sunPosID, sun_pos.x, sun_pos.y, sun_pos.z);

		//pass cameraPosition
		GLuint viewPosID = glGetUniformLocation(this->programID, "ViewPos");
		glUniform3f(viewPosID, view_pos.x, view_pos.y, view_pos.z);

		//vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

		//uvs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, this->uvBuffer);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

		//normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, this->normalBuffer);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_TRUE, 0, 0);

		//weights
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, this->weightsBuffer);
		glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, 0);

		//ids
		glEnableVertexAttribArray(4);
		glBindBuffer(GL_ARRAY_BUFFER, this->bonesIDBuffer);
		glVertexAttribIPointer(4, 4, GL_INT, 0, 0);

		//Model matrix
		GLuint MmatrixID = glGetUniformLocation(this->programID, "Mmatrix");
		glUniformMatrix4fv(MmatrixID, 1, GL_FALSE, &this->mMatrix[0][0]);
		

		//here get joint matrices to render and get inversed bindpose
		std::vector<glm::mat4>* joints = this->skeleton->GetActiveJoints();
		std::vector<glm::mat4>* bindPoseJoints = this->skeleton->GetInverseJoints();

		//JOINTS
		GLuint JointsID = glGetUniformLocation(this->programID, "Joints");
		glUniformMatrix4fv(JointsID, joints->size(), GL_FALSE, &joints->at(0)[0][0]);

		//INV TPOSE
		GLuint InvBPoseID = glGetUniformLocation(this->programID, "InvBPose");
		glUniformMatrix4fv(InvBPoseID, bindPoseJoints->size(), GL_FALSE, &bindPoseJoints->at(0)[0][0]);

		glDrawArrays(GL_TRIANGLES, 0, this->verticesCount);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);
		glDisableVertexAttribArray(4);
		CleanPipelineState();
	}
		
	
	if (this->drawMode == DrawMode::SKELETON || this->drawMode == DrawMode::MODEL_SKELETON) {
		this->skeleton->Render(p_mat, v_mat);
	}
}


/*Getter for skeleton*/
Skeleton* SkeletalModel::GetSkeleton() {

	return this->skeleton;
}




/*Updates matrix*/
void SkeletalModel::UpdateMatrix(){

	if (this->skeleton != NULL)
		this->skeleton->UpdateMatrix();

	//now update animations or something

	//now update normal matrix
	Model::UpdateMatrix();
}


/*Loads Skinned model from xml(OGRE IMPORTER), allows for max 4 bones asociated with one vertex*/
bool SkeletalModel::LoadModel(string file, bool has_transparency){

	std::vector<glm::vec3> indexed_vert;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	std::vector<glm::vec4> indexed_weights;
	std::vector<glm::ivec4> indexed_boneIds;
	string prevModelPath;
	string skeletonRelPath;


	prevModelPath = file;
	prevModelPath = prevModelPath.substr(0, prevModelPath.find_last_of("/") + 1);

	std::cout << "LOADING SKINNED MODEL" << std::endl;


	if (!SkeletalModel::OpenModel(file, indexed_vert, indexed_uvs, indexed_normals, indexed_weights, indexed_boneIds, skeletonRelPath)){
		return false;
	}
	
	//here load skeleton
	if (!this->skeleton->LoadModel(prevModelPath + skeletonRelPath + ".xml")) {
		std::cout << "Warning: wasnt able to load skeleton file, so no skeleton data are available" << std::endl;
		return false;
	}

	BufferData(indexed_vert, indexed_uvs, indexed_normals, indexed_weights, indexed_boneIds);

	this->modelPath = file;
	this->hasTransparency = has_transparency;
	this->modelLoaded = true;
	return true;
}


/*Opens file, and fills up the vectors*/
bool SkeletalModel::OpenModel(string filename, std::vector<glm::vec3>& vertOut, std::vector<glm::vec2>& uvOut, std::vector<glm::vec3>& normalOut, std::vector<glm::vec4>& weightsOut, std::vector<glm::ivec4>& bonesIdsOut, string& skeletonFileNameOut) {
	
	xmlDoc* document;

	xmlNode* root_element;
	xmlNode* currentTag;
	xmlChar* xmlArray;
	xmlNode* vertex;
	xmlNode* faces;


	long currentVertexIndex;
	long oldVertexIndex = -1;
	int weightNum = 0;
	long indice = 0;


	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	std::vector<long> indices;


	glm::vec3 vertexVec;
	glm::vec3 normalVec;
	glm::vec2 uvVec;
	std::vector<glm::vec4> weights;
	std::vector<glm::ivec4> ids;


	/**OPEN THE MODEL**/

	document = xmlReadFile(filename.c_str(), "UTF-8", 0);//XML_PARSE_RECOVER
	if (document == NULL){
		std::cout << "Provided path is not a valid XML" << std::endl;
		xmlCleanupParser();
		return false;
	}

	root_element = xmlDocGetRootElement(document);
	if (root_element == NULL){
		cout << "Provided path is not a valid XML" << endl;
		xmlFreeDoc(document);
		xmlCleanupParser();
	}


	/***LOAD VERTICES, UVS, NORMALS***/
	vertex = root_element->children->next->children->next->children->next;
	currentTag = vertex;

	while (1){
		//position in xml

		currentTag = vertex;
		currentTag = vertex->children->next;
		while (1){
			//new from here delete and replace with older if something is wrong
			if (!xmlStrcmp((const xmlChar *)"position", currentTag->name)){
				//vertices
				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"x");
				vertexVec.x = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"y");
				vertexVec.y = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"z");
				vertexVec.z = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				vertices.push_back(vertexVec);
			}
			else if (!xmlStrcmp((const xmlChar *)"normal", currentTag->name)){
				//normals
				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"x");
				normalVec.x = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"y");
				normalVec.y = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"z");
				normalVec.z = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				normals.push_back(normalVec);
			}
			else if (!xmlStrcmp((const xmlChar *)"texcoord", currentTag->name)){
				//uvs
				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"u");
				uvVec.x = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"v");
				uvVec.y = strtof((const char*)xmlArray, NULL);
				xmlFree(xmlArray);

				uvs.push_back(uvVec);
			}

			if (currentTag->next->next == NULL)
				break;
			currentTag = currentTag->next->next;
		}

		if (vertex->next->next == NULL)
			break;
		vertex = vertex->next->next;
	}

	/***LOAD FACES***/
	faces = root_element->children->next->next->next->children->next->children->next->children->next;
	while (1){

		xmlArray = xmlGetProp(faces, (const xmlChar *)"v1");
		indice = strtol((const char*)xmlArray, NULL, 10);
		indices.push_back(indice);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(faces, (const xmlChar *)"v2");
		indice = strtol((const char*)xmlArray, NULL, 10);
		indices.push_back(indice);
		xmlFree(xmlArray);

		xmlArray = xmlGetProp(faces, (const xmlChar *)"v3");
		indice = strtol((const char*)xmlArray, NULL, 10);
		indices.push_back(indice);
		xmlFree(xmlArray);

		if (faces->next->next == NULL)
			break;
		faces = faces->next->next;
	}

	long verticesCount = vertices.size();

	/***GETTING THE WEIGHTS AND IDS***/

	//load the bone asigments and boneweights(max 3 bones per vertex) 
	//search for  <boneassignments>

	for (int i = 0; i != verticesCount; ++i){
		weights.push_back(glm::vec4(0.f, 0.f, 0.f, 0.f));
		ids.push_back(glm::ivec4(0, 0, 0, 0));
	}

	currentTag = root_element->children->next; //init the currentTag 

	while (1){
		//also get name of targeted skeleton
		if (!xmlStrcmp((const xmlChar *)"skeletonlink", currentTag->name)) {
			xmlArray = xmlGetProp(currentTag, (const xmlChar *)"name");
			if (xmlArray != NULL) {
				skeletonFileNameOut = string((const char*)xmlArray);
			}
			xmlFree(xmlArray);
		}

		if (!xmlStrcmp((const xmlChar *)"boneassignments", currentTag->name)){
			//found boneassigments, there still doesnt have to be anything
			if (currentTag->children->next == NULL){
				break;
			}

			currentTag = currentTag->children->next;
			//found it, get the info
			while (1){
				xmlArray = xmlGetProp(currentTag, (const xmlChar *)"vertexindex");
				currentVertexIndex = strtol((const char*)xmlArray, NULL, 10);
				xmlFree(xmlArray);
				if (currentVertexIndex == oldVertexIndex){
					++weightNum;
				}
				else{
					weightNum = 0;
				}


				switch (weightNum){
					case 0:{
						xmlArray = xmlGetProp(currentTag, (const xmlChar *)"weight");
						weights[currentVertexIndex].x = strtof((const char*)xmlArray, NULL);
						xmlFree(xmlArray);
						xmlArray = xmlGetProp(currentTag, (const xmlChar *)"boneindex");
						ids[currentVertexIndex].x = strtol((const char*)xmlArray, NULL, 10);
						xmlFree(xmlArray);
						break;
					}
					case 1:{
						xmlArray = xmlGetProp(currentTag, (const xmlChar *)"weight");
						weights[currentVertexIndex].y = strtof((const char*)xmlArray, NULL);
						xmlFree(xmlArray);
						xmlArray = xmlGetProp(currentTag, (const xmlChar *)"boneindex");
						ids[currentVertexIndex].y = strtol((const char*)xmlArray, NULL, 10);
						xmlFree(xmlArray);
						break;
					}
					case 2:{
						xmlArray = xmlGetProp(currentTag, (const xmlChar *)"weight");
						weights[currentVertexIndex].z = strtof((const char*)xmlArray, NULL);
						xmlFree(xmlArray);
						xmlArray = xmlGetProp(currentTag, (const xmlChar *)"boneindex");
						ids[currentVertexIndex].z = strtol((const char*)xmlArray, NULL, 10);
						xmlFree(xmlArray);
						break;
					}
					case 3: {
						xmlArray = xmlGetProp(currentTag, (const xmlChar *)"weight");
						weights[currentVertexIndex].w = strtof((const char*)xmlArray, NULL);
						xmlFree(xmlArray);
						xmlArray = xmlGetProp(currentTag, (const xmlChar *)"boneindex");
						ids[currentVertexIndex].w = strtol((const char*)xmlArray, NULL, 10);
						xmlFree(xmlArray);
						break;
					}
					default:{
						cout << "ERR, MORE THAN 4 WEIGHTS PER VERTEX" << endl;
						break;
					}
				}

				oldVertexIndex = currentVertexIndex;
				if (currentTag->next->next == NULL){
					break;
				}
				currentTag = currentTag->next->next;
			}
		}
		if (currentTag->next->next == NULL){ //if there isnt req tag, end it
			break;
		}
		currentTag = currentTag->next->next; //move to next
	}


	/***INDEXING***/

	for (size_t i = 0; i < indices.size(); ++i){

		long index = indices[i];

		glm::vec3 indexed_vertex = vertices[index];
		glm::vec2 indexed_uv = uvs[index];
		glm::vec3 indexed_normal = normals[index];

		bonesIdsOut.push_back(ids[index]);
		weightsOut.push_back(weights[index]);
		vertOut.push_back(indexed_vertex);
		uvOut.push_back(indexed_uv);
		normalOut.push_back(indexed_normal);
	}

	/**CLEANUP PARSER**/
	xmlFreeDoc(document);
	xmlCleanupParser();

	//test if was succesfull
	if (weightsOut.size() == 0){
		return false;
	}

	this->modelLoaded = true;
	return true;
}


/*Loads texture*/
bool SkeletalModel::LoadTexture(string file){

	return Model::LoadTexture(file);
}


/*Buffers data from skeletal model to gpu*/
void SkeletalModel::BufferData(std::vector<glm::vec3>& vertices, std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals, std::vector<glm::vec4>& weights, std::vector<glm::ivec4>& boneIds){

	glBindBuffer(GL_ARRAY_BUFFER, this->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW); //fill buffer with vertices

	glBindBuffer(GL_ARRAY_BUFFER, this->uvBuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW); //fill buffer with uvs

	glBindBuffer(GL_ARRAY_BUFFER, this->normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW); //fill buffer with normals

	glBindBuffer(GL_ARRAY_BUFFER, this->weightsBuffer);
	glBufferData(GL_ARRAY_BUFFER, weights.size() * sizeof(glm::vec4), &weights[0], GL_STATIC_DRAW); //fill buffer with weights

	glBindBuffer(GL_ARRAY_BUFFER, this->bonesIDBuffer);
	glBufferData(GL_ARRAY_BUFFER, boneIds.size() * sizeof(glm::ivec4), &boneIds[0], GL_STATIC_DRAW); //fill buffer with boneIds

	//vertices																								
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//uvs
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	//normals
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_TRUE, 0, 0);

	//weights
	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, 0);
	
	//boneIds
	glEnableVertexAttribArray(4);
	glVertexAttribIPointer(4, 4, GL_INT, 0, 0);

	this->verticesCount = vertices.size();
	this->modelLoaded = true;
}



void SkeletalModel::SetPipelineState(){

	Model::SetPipelineState();
}


void SkeletalModel::CleanPipelineState(){

	Model::CleanPipelineState();
}


bool SkeletalModel::LoadTextureInternal(string file, GLuint texture){

	return Model::LoadTextureInternal(file, texture);
}


SkeletalModel::~SkeletalModel(){

	delete skeleton;
}
