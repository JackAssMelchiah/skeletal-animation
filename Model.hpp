/**********************************
* Model.hpp
* Class model holds info about model used in scene(such as vertices, ..., textures, colision bodies,..) 
* It is possible o change camera type to fps(used for capturing images within game) within coresponding scene
* for more info about class mehods, please check out .cpp file
*/



#ifndef MODEL_HPP
#define MODEL_HPP

#include "BaseObject.hpp"
#include "Shader.hpp" 

#include <GL/glew.h>//before opengl
#include <glm.hpp>
#include <SDL_image.h>
#include <gtc/matrix_transform.hpp>
#include <gtx/quaternion.hpp>
#include <gtc/quaternion.hpp>

#include <libxml/parser.h>
#include <libxml/tree.h>


#include <iostream>
#include <vector>
#include <string>


class Model: public BaseObject{

public:
	Model();
	Model(string vs, string gs, string fs);
	virtual void GlReqInit();
	void SetPosition(glm::vec3);
	void SetRotation(glm::vec3);
	void SetScale(glm::vec3);
	void SetAngle(float);
	glm::vec3 GetPosition();
	glm::vec3 GetRotation();
	glm::vec3 GetScale();
	float GetAngle();
	void SetAddinationalRotation(glm::quat q);
	glm::quat GetAddinationalRotation();
	GLuint BindShaders(GLuint shaders);
	virtual void Render(glm::mat4& p_mat, glm::mat4& v_mat, glm::vec3 & sun, glm::vec3 & sun_pos, glm::vec3 view_pos);
	virtual void UpdateMatrix();
	virtual bool LoadModel(string file, bool has_transparency);
	bool OpenXMLModel(string filename, std::vector<glm::vec3>& vertOut, std::vector<glm::vec2>& uvOut, std::vector<glm::vec3>& normalOut);
	virtual bool LoadTexture(string file);
	bool HasTransparency();
	string GetTexturePath();
	string GetModelPath();
	glm::mat4 GetModelMatrix();
	void SetName(string s);
	string GetName();
	int GetSceneIndex();
	void SetSceneIndex(int index);
	
	virtual ~Model();
protected:
	Model(bool);
	/*methods*/
	void DoIndexing(std::vector<glm::vec3>& vertices, std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals, std::vector<int>& indices, std::vector<glm::vec3>& indexed_vert, std::vector<glm::vec2>& indexed_uvs, std::vector<glm::vec3>& indexed_normals);
	virtual void BufferData(std::vector<glm::vec3>&, std::vector<glm::vec2>&, std::vector<glm::vec3>&);
	void SaveGpuBufferHandler(GLuint);
	virtual void SetPipelineState();
	virtual void CleanPipelineState();
	virtual bool LoadTextureInternal(string file, GLuint texture);
	
	bool LoadShaders(string v_file, string g_file, string f_dfile);

	/*attributes*/
	glm::mat4 mMatrix;
	glm::vec3 location;
	glm::vec3 rotation;
	glm::vec3 scale;
	float angle;
	glm::quat ad_quat;
	std::vector<GLuint> vboList;
	string name;
	int cpuIndexScene;
	

	bool active;

	GLuint vaoID;
	GLuint programID;
	GLuint texture;
	GLuint vertexBuffer;
	GLuint uvBuffer;
	GLuint normalBuffer;
	GLuint ebo;

	int verticesCount; 
	bool shadersLoaded;
	
	bool modelLoaded;
	bool textureLoaded;
	bool hasTransparency;

	string texturePath;
	string modelPath;
	Shader shader;
	int indexWithinScene;


	private:
		void DataInit(); //this isnt and shouldnt be virtual!!

};


#endif